# HPC1 CLUSTER necessary exports
# DO NOT RUN WITH THE OMPI MODULE LOADED

module load gcc/7.3.0
module load mkl/2018.1
module load impi/2018.1

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cluster/parallel_studio/2018.1/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cluster/parallel_studio/2018.1/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64/
