########################################################################
This is the DARPA/DOE HPC Challenge Benchmark version 1.5.0 October 2012
Produced by Jack Dongarra and Piotr Luszczek
Innovative Computing Laboratory
University of Tennessee Knoxville and Oak Ridge National Laboratory

See the source files for authors of specific codes.
Compiled on May 30 2018 at 21:47:54
Current time (1527730060) is Wed May 30 22:27:40 2018

Hostname: 'hpc1-ib'
########################################################################
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       8 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Begin of MPIRandomAccess section.
Running on 64 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^24 = 16777216 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 1679193664 (for a TIME BOUND of 60.00 secs)
CPU time used = 29.499110 seconds
Real time used = 33.743958 seconds
0.049762795 Billion(10^9) Updates    per second [GUP/s]
0.000777544 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 2.829089 seconds
Verification:  Real time used = 2.842856 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527730099) is Wed May 30 22:28:19 2018

End of MPIRandomAccess section.
Begin of StarRandomAccess section.
Main table size   = 2^24 = 16777216 words
Number of updates = 67108864
CPU time used  = 1.947692 seconds
Real time used = 1.949800 seconds
0.034418332 Billion(10^9) Updates    per second [GUP/s]
Found 0 errors in 16777216 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.031813
Average GUP/s 0.034351
Maximum GUP/s 0.036423
Current time (1527730103) is Wed May 30 22:28:23 2018

End of StarRandomAccess section.
Begin of SingleRandomAccess section.
Node(s) with error 0
Node selected 8
Single GUP/s 0.091275
Current time (1527730104) is Wed May 30 22:28:24 2018

End of SingleRandomAccess section.
Begin of MPIRandomAccess_LCG section.
Running on 64 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^24 = 16777216 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 2965291968 (for a TIME BOUND of 60.00 secs)
CPU time used = 52.141436 seconds
Real time used = 59.611258 seconds
0.049743825 Billion(10^9) Updates    per second [GUP/s]
0.000777247 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 4.519857 seconds
Verification:  Real time used = 4.533257 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527730169) is Wed May 30 22:29:29 2018

End of MPIRandomAccess_LCG section.
Begin of StarRandomAccess_LCG section.
Main table size   = 2^24 = 16777216 words
Number of updates = 67108864
CPU time used  = 1.970218 seconds
Real time used = 1.971976 seconds
0.034031282 Billion(10^9) Updates    per second [GUP/s]
Found 0 errors in 16777216 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.031403
Average GUP/s 0.034259
Maximum GUP/s 0.036118
Current time (1527730174) is Wed May 30 22:29:34 2018

End of StarRandomAccess_LCG section.
Begin of SingleRandomAccess_LCG section.
Node(s) with error 0
Node selected 37
Single GUP/s 0.092275
Current time (1527730175) is Wed May 30 22:29:35 2018

End of SingleRandomAccess_LCG section.
Begin of PTRANS section.
M: 23040
N: 23040
MB: 512
NB: 512
P: 8
Q: 8
TIME   M     N    MB  NB  P   Q     TIME   CHECK   GB/s   RESID
---- ----- ----- --- --- --- --- -------- ------ -------- -----
WALL 23040 23040 512 512   8   8     0.53 PASSED    7.957  0.00
CPU  23040 23040 512 512   8   8     0.52 PASSED    8.143  0.00
WALL 23040 23040 512 512   8   8     0.57 PASSED    7.460  0.00
CPU  23040 23040 512 512   8   8     0.56 PASSED    7.555  0.00
WALL 23040 23040 512 512   8   8     0.54 PASSED    7.460  0.00
CPU  23040 23040 512 512   8   8     0.53 PASSED    7.952  0.00
WALL 23040 23040 512 512   8   8     0.54 PASSED    7.460  0.00
CPU  23040 23040 512 512   8   8     0.54 PASSED    7.887  0.00
WALL 23040 23040 512 512   8   8     0.53 PASSED    7.460  0.00
CPU  23040 23040 512 512   8   8     0.52 PASSED    8.129  0.00

Finished    5 tests, with the following results:
    5 tests completed and passed residual checks.
    0 tests completed and failed residual checks.
    0 tests skipped because of illegal input values.

END OF TESTS.
Current time (1527730185) is Wed May 30 22:29:45 2018

End of PTRANS section.
Begin of StarDGEMM section.
Scaled residual: 0.0107861
Node(s) with error 0
Minimum Gflop/s 19.691025
Average Gflop/s 21.457602
Maximum Gflop/s 23.214038
Current time (1527730189) is Wed May 30 22:29:49 2018

End of StarDGEMM section.
Begin of SingleDGEMM section.
Node(s) with error 0
Node selected 29
Single DGEMM Gflop/s 25.922084
Current time (1527730193) is Wed May 30 22:29:53 2018

End of SingleDGEMM section.
Begin of StarSTREAM section.
-------------------------------------------------------------
This system uses 8 bytes per DOUBLE PRECISION word.
-------------------------------------------------------------
Array size = 11059200, Offset = 0
Total memory required = 0.2472 GiB.
Each test is run 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
The SCALAR value used for this run is 0.420000
-------------------------------------------------------------
Number of Threads requested = 1
-------------------------------------------------------------
Your clock granularity/precision appears to be 1 microseconds.
Each test below will take on the order of 31450 microseconds.
   (= 31450 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
VERBOSE: total setup time for rank 0 = 0.216758 seconds
-------------------------------------------------------------
Function      Rate (GB/s)   Avg time     Min time     Max time
Copy:           4.1632       0.0445       0.0425       0.0554
Scale:          3.6348       0.0495       0.0487       0.0507
Add:            4.1338       0.0653       0.0642       0.0723
Triad:          4.1775       0.0653       0.0635       0.0712
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays
-------------------------------------------------------------
Node(s) with error 0
Minimum Copy GB/s 4.163181
Average Copy GB/s 4.163181
Maximum Copy GB/s 4.163181
Minimum Scale GB/s 3.634758
Average Scale GB/s 3.634758
Maximum Scale GB/s 3.634758
Minimum Add GB/s 4.133763
Average Add GB/s 4.133763
Maximum Add GB/s 4.133763
Minimum Triad GB/s 4.177491
Average Triad GB/s 4.177491
Maximum Triad GB/s 4.177491
Current time (1527730196) is Wed May 30 22:29:56 2018

End of StarSTREAM section.
Begin of SingleSTREAM section.
Node(s) with error 0
Node selected 61
Single STREAM Copy GB/s 5.832930
Single STREAM Scale GB/s 13.107224
Single STREAM Add GB/s 12.502168
Single STREAM Triad GB/s 12.451687
Current time (1527730197) is Wed May 30 22:29:57 2018

End of SingleSTREAM section.
Begin of MPIFFT section.
Number of nodes: 64
Vector size:            134217728
Generation time:     0.063
Tuning:     0.027
Computing:     0.794
Inverse FFT:     0.821
max(|x-x0|): 4.707e-15
Gflop/s:    22.815
Current time (1527730198) is Wed May 30 22:29:58 2018

End of MPIFFT section.
Begin of StarFFT section.
Vector size: 4194304
Generation time:     0.123
Tuning:     0.000
Computing:     0.226
Inverse FFT:     0.280
max(|x-x0|): 4.246e-15
Node(s) with error 0
Minimum Gflop/s 1.765130
Average Gflop/s 1.903925
Maximum Gflop/s 2.059208
Current time (1527730199) is Wed May 30 22:29:59 2018

End of StarFFT section.
Begin of SingleFFT section.
Node(s) with error 0
Node selected 2
Single FFT Gflop/s 2.723318
Current time (1527730200) is Wed May 30 22:30:00 2018

End of SingleFFT section.
Begin of LatencyBandwidth section.

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Details - level 2
-----------------

MPI_Wtime granularity.
Max. MPI_Wtick is 0.000002 sec
wtick is set to   0.000002 sec  

Message Length: 8
Latency   min / avg / max:   0.001043 /   0.001043 /   0.001043 msecs
Bandwidth min / avg / max:      7.670 /      7.670 /      7.670 MByte/s

Use MPI_Wtick for estimation of max pairs
message size:                                  8
max time :                             10.000000 secs
latency for msg:                        0.001043 msecs
estimation for ping pong:               0.093877 msecs
max number of ping pong pairs       =     100000
max client pings = max server pongs =        316
stride for latency                  =          1
Message Length: 8
Latency   min / avg / max:   0.000199 /   0.000919 /   0.001417 msecs
Bandwidth min / avg / max:      5.647 /     11.458 /     40.265 MByte/s

Message Length: 2000000
Latency   min / avg / max:   0.524044 /   0.524044 /   0.524044 msecs
Bandwidth min / avg / max:   3816.473 /   3816.473 /   3816.473 MByte/s

MPI_Wtime granularity is ok.
message size:                            2000000
max time :                             30.000000 secs
latency for msg:                        0.524044 msecs
estimation for ping pong:               4.192352 msecs
max number of ping pong pairs       =       7155
max client pings = max server pongs =         84
stride for latency                  =          1
Message Length: 2000000
Latency   min / avg / max:   0.106573 /   0.340834 /   0.553966 msecs
Bandwidth min / avg / max:   3610.333 /   8911.636 /  18766.461 MByte/s

Message Size:                           8 Byte
Natural Order Latency:           0.001160 msec
Natural Order Bandwidth:         6.899042 MB/s
Avg Random Order Latency:        0.003364 msec
Avg Random Order Bandwidth:      2.377883 MB/s

Message Size:                     2000000 Byte
Natural Order Latency:           1.832724 msec
Natural Order Bandwidth:      1091.272018 MB/s
Avg Random Order Latency:        6.489969 msec
Avg Random Order Bandwidth:    308.167902 MB/s

Execution time (wall clock)      =    18.265 sec on 64 processes
 - for cross ping_pong latency   =     2.435 sec
 - for cross ping_pong bandwidth =    13.299 sec
 - for ring latency              =     0.107 sec
 - for ring bandwidth            =     2.424 sec

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Major Benchmark results:
------------------------

Max Ping Pong Latency:                 0.001417 msecs
Randomly Ordered Ring Latency:         0.003364 msecs
Min Ping Pong Bandwidth:            3610.332688 MB/s
Naturally Ordered Ring Bandwidth:   1091.272018 MB/s
Randomly  Ordered Ring Bandwidth:    308.167902 MB/s

------------------------------------------------------------------

Detailed benchmark results:
Ping Pong:
Latency   min / avg / max:   0.000199 /   0.000919 /   0.001417 msecs
Bandwidth min / avg / max:   3610.333 /   8911.636 /  18766.461 MByte/s
Ring:
On naturally ordered ring: latency=      0.001160 msec, bandwidth=   1091.272018 MB/s
On randomly  ordered ring: latency=      0.003364 msec, bandwidth=    308.167902 MB/s

------------------------------------------------------------------

Benchmark conditions:
 The latency   measurements were done with        8 bytes
 The bandwidth measurements were done with  2000000 bytes
 The ring communication was done in both directions on 64 processes
 The Ping Pong measurements were done on 
  -        4032 pairs of processes for latency benchmarking, and 
  -        4032 pairs of processes for bandwidth benchmarking, 
 out of 64*(64-1) =       4032 possible combinations on 64 processes.
 (1 MB/s = 10**6 byte/sec)

------------------------------------------------------------------
Current time (1527730218) is Wed May 30 22:30:18 2018

End of LatencyBandwidth section.
Begin of HPL section.
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       8 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11R6C8       46080   512     8     8              56.51              1.154e+03
--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0031916 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
Current time (1527730276) is Wed May 30 22:31:16 2018

End of HPL section.
Begin of Summary section.
VersionMajor=1
VersionMinor=5
VersionMicro=0
VersionRelease=f
LANG=C
Success=1
sizeof_char=1
sizeof_short=2
sizeof_int=4
sizeof_long=8
sizeof_void_ptr=8
sizeof_size_t=8
sizeof_float=4
sizeof_double=8
sizeof_s64Int=8
sizeof_u64Int=8
sizeof_struct_double_double=16
CommWorldProcs=64
MPI_Wtick=1.000000e-06
HPL_Tflops=1.15444
HPL_time=56.5063
HPL_eps=1.11022e-16
HPL_RnormI=6.20567e-10
HPL_Anorm1=11637.5
HPL_AnormI=11641.7
HPL_Xnorm1=29866.1
HPL_XnormI=3.26464
HPL_BnormI=0.499991
HPL_N=46080
HPL_NB=512
HPL_nprow=8
HPL_npcol=8
HPL_depth=1
HPL_nbdiv=6
HPL_nbmin=8
HPL_cpfact=C
HPL_crfact=R
HPL_ctop=1
HPL_order=R
HPL_dMACH_EPS=1.110223e-16
HPL_dMACH_SFMIN=2.225074e-308
HPL_dMACH_BASE=2.000000e+00
HPL_dMACH_PREC=2.220446e-16
HPL_dMACH_MLEN=5.300000e+01
HPL_dMACH_RND=1.000000e+00
HPL_dMACH_EMIN=-1.021000e+03
HPL_dMACH_RMIN=2.225074e-308
HPL_dMACH_EMAX=1.024000e+03
HPL_dMACH_RMAX=1.797693e+308
HPL_sMACH_EPS=5.960464e-08
HPL_sMACH_SFMIN=1.175494e-38
HPL_sMACH_BASE=2.000000e+00
HPL_sMACH_PREC=1.192093e-07
HPL_sMACH_MLEN=2.400000e+01
HPL_sMACH_RND=1.000000e+00
HPL_sMACH_EMIN=-1.250000e+02
HPL_sMACH_RMIN=1.175494e-38
HPL_sMACH_EMAX=1.280000e+02
HPL_sMACH_RMAX=3.402823e+38
dweps=1.110223e-16
sweps=5.960464e-08
HPLMaxProcs=64
HPLMinProcs=64
DGEMM_N=3325
StarDGEMM_Gflops=21.4576
SingleDGEMM_Gflops=25.9221
PTRANS_GBs=7.45968
PTRANS_time=0.529354
PTRANS_residual=0
PTRANS_n=23040
PTRANS_nb=512
PTRANS_nprow=8
PTRANS_npcol=8
MPIRandomAccess_LCG_N=1073741824
MPIRandomAccess_LCG_time=59.6113
MPIRandomAccess_LCG_CheckTime=4.53326
MPIRandomAccess_LCG_Errors=0
MPIRandomAccess_LCG_ErrorsFraction=0
MPIRandomAccess_LCG_ExeUpdates=2965291968
MPIRandomAccess_LCG_GUPs=0.0497438
MPIRandomAccess_LCG_TimeBound=60
MPIRandomAccess_LCG_Algorithm=0
MPIRandomAccess_N=1073741824
MPIRandomAccess_time=33.744
MPIRandomAccess_CheckTime=2.84286
MPIRandomAccess_Errors=0
MPIRandomAccess_ErrorsFraction=0
MPIRandomAccess_ExeUpdates=1679193664
MPIRandomAccess_GUPs=0.0497628
MPIRandomAccess_TimeBound=60
MPIRandomAccess_Algorithm=0
RandomAccess_LCG_N=16777216
StarRandomAccess_LCG_GUPs=0.0342591
SingleRandomAccess_LCG_GUPs=0.0922749
RandomAccess_N=16777216
StarRandomAccess_GUPs=0.0343507
SingleRandomAccess_GUPs=0.0912745
STREAM_VectorSize=11059200
STREAM_Threads=1
StarSTREAM_Copy=4.16318
StarSTREAM_Scale=3.63476
StarSTREAM_Add=4.13376
StarSTREAM_Triad=4.17749
SingleSTREAM_Copy=5.83293
SingleSTREAM_Scale=13.1072
SingleSTREAM_Add=12.5022
SingleSTREAM_Triad=12.4517
FFT_N=4194304
StarFFT_Gflops=1.90392
SingleFFT_Gflops=2.72332
MPIFFT_N=134217728
MPIFFT_Gflops=22.8148
MPIFFT_maxErr=4.70749e-15
MPIFFT_Procs=64
MaxPingPongLatency_usec=1.4166
RandomlyOrderedRingLatency_usec=3.36434
MinPingPongBandwidth_GBytes=3.61033
NaturallyOrderedRingBandwidth_GBytes=1.09127
RandomlyOrderedRingBandwidth_GBytes=0.308168
MinPingPongLatency_usec=0.198682
AvgPingPongLatency_usec=0.91943
MaxPingPongBandwidth_GBytes=18.7665
AvgPingPongBandwidth_GBytes=8.91164
NaturallyOrderedRingLatency_usec=1.15958
FFTEnblk=16
FFTEnp=8
FFTEl2size=1048576
M_OPENMP=201611
omp_get_num_threads=1
omp_get_max_threads=1
omp_get_num_procs=1
MemProc=-1
MemSpec=-1
MemVal=-1
MPIFFT_time0=2.86102e-06
MPIFFT_time1=0.171654
MPIFFT_time2=0.067086
MPIFFT_time3=0.187243
MPIFFT_time4=0.17151
MPIFFT_time5=0.179632
MPIFFT_time6=1.90735e-06
CPS_HPCC_FFT_235=0
CPS_HPCC_FFTW_ESTIMATE=0
CPS_HPCC_MEMALLCTR=0
CPS_HPL_USE_GETPROCESSTIMES=0
CPS_RA_SANDIA_NOPT=0
CPS_RA_SANDIA_OPT2=0
CPS_USING_FFTW=0
End of Summary section.
########################################################################
End of HPC Challenge tests.
Current time (1527730276) is Wed May 30 22:31:16 2018

########################################################################
