########################################################################
This is the DARPA/DOE HPC Challenge Benchmark version 1.5.0 October 2012
Produced by Jack Dongarra and Piotr Luszczek
Innovative Computing Laboratory
University of Tennessee Knoxville and Oak Ridge National Laboratory

See the source files for authors of specific codes.
Compiled on May 30 2018 at 22:45:59
Current time (1527731164) is Wed May 30 22:46:04 2018

Hostname: 'hpc1-ib'
########################################################################
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       2 
Q      :       4 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Begin of MPIRandomAccess section.
Running on 8 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^27 = 134217728 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 925359288 (for a TIME BOUND of 60.00 secs)
CPU time used = 43.266612 seconds
Real time used = 59.596903 seconds
0.015526969 Billion(10^9) Updates    per second [GUP/s]
0.001940871 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 2.780236 seconds
Verification:  Real time used = 2.781240 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527731229) is Wed May 30 22:47:09 2018

End of MPIRandomAccess section.
Begin of StarRandomAccess section.
Main table size   = 2^27 = 134217728 words
Number of updates = 536870912
CPU time used  = 43.677119 seconds
Real time used = 5.470149 seconds
0.098145573 Billion(10^9) Updates    per second [GUP/s]
Found 6866 errors in 134217728 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.080918
Average GUP/s 0.088894
Maximum GUP/s 0.099559
Current time (1527731242) is Wed May 30 22:47:22 2018

End of StarRandomAccess section.
Begin of SingleRandomAccess section.
Node(s) with error 0
Node selected 6
Single GUP/s 0.081245
Current time (1527731254) is Wed May 30 22:47:34 2018

End of SingleRandomAccess section.
Begin of MPIRandomAccess_LCG section.
Running on 8 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^27 = 134217728 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 996373776 (for a TIME BOUND of 60.00 secs)
CPU time used = 42.709331 seconds
Real time used = 60.013316 seconds
0.016602545 Billion(10^9) Updates    per second [GUP/s]
0.002075318 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 2.891315 seconds
Verification:  Real time used = 2.891376 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527731320) is Wed May 30 22:48:40 2018

End of MPIRandomAccess_LCG section.
Begin of StarRandomAccess_LCG section.
Main table size   = 2^27 = 134217728 words
Number of updates = 536870912
CPU time used  = 42.917878 seconds
Real time used = 5.374931 seconds
0.099884241 Billion(10^9) Updates    per second [GUP/s]
Found 39 errors in 134217728 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.083035
Average GUP/s 0.091005
Maximum GUP/s 0.102041
Current time (1527731332) is Wed May 30 22:48:52 2018

End of StarRandomAccess_LCG section.
Begin of SingleRandomAccess_LCG section.
Node(s) with error 0
Node selected 4
Single GUP/s 0.082974
Current time (1527731345) is Wed May 30 22:49:05 2018

End of SingleRandomAccess_LCG section.
Begin of PTRANS section.
M: 23040
N: 23040
MB: 512
NB: 512
P: 2
Q: 4
TIME   M     N    MB  NB  P   Q     TIME   CHECK   GB/s   RESID
---- ----- ----- --- --- --- --- -------- ------ -------- -----
WALL 23040 23040 512 512   2   4     0.82 PASSED    5.168  0.00
CPU  23040 23040 512 512   2   4     0.77 PASSED    5.486  0.00
WALL 23040 23040 512 512   2   4     0.82 PASSED    5.168  0.00
CPU  23040 23040 512 512   2   4     0.77 PASSED    5.522  0.00
WALL 23040 23040 512 512   2   4     0.90 PASSED    4.699  0.00
CPU  23040 23040 512 512   2   4     0.80 PASSED    5.333  0.00
WALL 23040 23040 512 512   2   4     0.91 PASSED    4.658  0.00
CPU  23040 23040 512 512   2   4     0.81 PASSED    5.254  0.00
WALL 23040 23040 512 512   2   4     0.81 PASSED    4.658  0.00
CPU  23040 23040 512 512   2   4     0.77 PASSED    5.506  0.00

Finished    5 tests, with the following results:
    5 tests completed and passed residual checks.
    0 tests completed and failed residual checks.
    0 tests skipped because of illegal input values.

END OF TESTS.
Current time (1527731387) is Wed May 30 22:49:47 2018

End of PTRANS section.
Begin of StarDGEMM section.
Scaled residual: 0.00654248
Node(s) with error 0
Minimum Gflop/s 184.027685
Average Gflop/s 189.135615
Maximum Gflop/s 194.199192
Current time (1527731404) is Wed May 30 22:50:04 2018

End of StarDGEMM section.
Begin of SingleDGEMM section.
Node(s) with error 0
Node selected 3
Single DGEMM Gflop/s 206.278185
Current time (1527731419) is Wed May 30 22:50:19 2018

End of SingleDGEMM section.
Begin of StarSTREAM section.
-------------------------------------------------------------
This system uses 8 bytes per DOUBLE PRECISION word.
-------------------------------------------------------------
Array size = 88473600, Offset = 0
Total memory required = 1.9775 GiB.
Each test is run 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
The SCALAR value used for this run is 0.420000
-------------------------------------------------------------
Number of Threads requested = 8
-------------------------------------------------------------
Your clock granularity/precision appears to be 1 microseconds.
Each test below will take on the order of 34603 microseconds.
   (= 34603 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
VERBOSE: total setup time for rank 0 = 0.196358 seconds
-------------------------------------------------------------
Function      Rate (GB/s)   Avg time     Min time     Max time
Copy:           5.8083       0.2439       0.2437       0.2443
Scale:         13.3540       0.1061       0.1060       0.1065
Add:           13.0326       0.1631       0.1629       0.1633
Triad:         13.0008       0.1637       0.1633       0.1644
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays
-------------------------------------------------------------
Node(s) with error 0
Minimum Copy GB/s 5.808263
Average Copy GB/s 5.808263
Maximum Copy GB/s 5.808263
Minimum Scale GB/s 13.354002
Average Scale GB/s 13.354002
Maximum Scale GB/s 13.354002
Minimum Add GB/s 13.032555
Average Add GB/s 13.032555
Maximum Add GB/s 13.032555
Minimum Triad GB/s 13.000784
Average Triad GB/s 13.000784
Maximum Triad GB/s 13.000784
Current time (1527731427) is Wed May 30 22:50:27 2018

End of StarSTREAM section.
Begin of SingleSTREAM section.
Node(s) with error 0
Node selected 1
Single STREAM Copy GB/s 38.444960
Single STREAM Scale GB/s 29.747648
Single STREAM Add GB/s 34.560005
Single STREAM Triad GB/s 34.522896
Current time (1527731429) is Wed May 30 22:50:29 2018

End of SingleSTREAM section.
Begin of MPIFFT section.
Number of nodes: 8
Vector size:            134217728
Generation time:     0.427
Tuning:     0.121
Computing:     1.733
Inverse FFT:     1.686
max(|x-x0|): 4.260e-15
Gflop/s:    10.453
Current time (1527731434) is Wed May 30 22:50:34 2018

End of MPIFFT section.
Begin of StarFFT section.
Vector size: 33554432
Generation time:     0.846
Tuning:     0.000
Computing:     0.308
Inverse FFT:     0.423
max(|x-x0|): 2.136e-15
Node(s) with error 0
Minimum Gflop/s 2.364665
Average Gflop/s 8.099558
Maximum Gflop/s 14.042843
Current time (1527731440) is Wed May 30 22:50:40 2018

End of StarFFT section.
Begin of SingleFFT section.
Node(s) with error 0
Node selected 2
Single FFT Gflop/s 13.586158
Current time (1527731442) is Wed May 30 22:50:42 2018

End of SingleFFT section.
Begin of LatencyBandwidth section.

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Details - level 2
-----------------

MPI_Wtime granularity.
Max. MPI_Wtick is 0.000001 sec
wtick is set to   0.000001 sec  

Message Length: 8
Latency   min / avg / max:   0.001118 /   0.001118 /   0.001118 msecs
Bandwidth min / avg / max:      7.158 /      7.158 /      7.158 MByte/s

MPI_Wtime granularity is ok.
message size:                                  8
max time :                             10.000000 secs
latency for msg:                        0.001118 msecs
estimation for ping pong:               0.100583 msecs
max number of ping pong pairs       =      99420
max client pings = max server pongs =        315
stride for latency                  =          1
Message Length: 8
Latency   min / avg / max:   0.000536 /   0.001015 /   0.001450 msecs
Bandwidth min / avg / max:      5.516 /      8.674 /     14.913 MByte/s

Message Length: 2000000
Latency   min / avg / max:   0.523567 /   0.523567 /   0.523567 msecs
Bandwidth min / avg / max:   3819.949 /   3819.949 /   3819.949 MByte/s

MPI_Wtime granularity is ok.
message size:                            2000000
max time :                             30.000000 secs
latency for msg:                        0.523567 msecs
estimation for ping pong:               4.188538 msecs
max number of ping pong pairs       =       7162
max client pings = max server pongs =         84
stride for latency                  =          1
Message Length: 2000000
Latency   min / avg / max:   0.133038 /   0.364846 /   0.539541 msecs
Bandwidth min / avg / max:   3706.853 /   8030.755 /  15033.348 MByte/s

Message Size:                           8 Byte
Natural Order Latency:           0.001001 msec
Natural Order Bandwidth:         7.989150 MB/s
Avg Random Order Latency:        0.001065 msec
Avg Random Order Bandwidth:      7.508253 MB/s

Message Size:                     2000000 Byte
Natural Order Latency:           0.583708 msec
Natural Order Bandwidth:      3426.369039 MB/s
Avg Random Order Latency:        0.863286 msec
Avg Random Order Bandwidth:   2316.730042 MB/s

Execution time (wall clock)      =     0.604 sec on 8 processes
 - for cross ping_pong latency   =     0.009 sec
 - for cross ping_pong bandwidth =     0.195 sec
 - for ring latency              =     0.011 sec
 - for ring bandwidth            =     0.389 sec

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Major Benchmark results:
------------------------

Max Ping Pong Latency:                 0.001450 msecs
Randomly Ordered Ring Latency:         0.001065 msecs
Min Ping Pong Bandwidth:            3706.852850 MB/s
Naturally Ordered Ring Bandwidth:   3426.369039 MB/s
Randomly  Ordered Ring Bandwidth:   2316.730042 MB/s

------------------------------------------------------------------

Detailed benchmark results:
Ping Pong:
Latency   min / avg / max:   0.000536 /   0.001015 /   0.001450 msecs
Bandwidth min / avg / max:   3706.853 /   8030.755 /  15033.348 MByte/s
Ring:
On naturally ordered ring: latency=      0.001001 msec, bandwidth=   3426.369039 MB/s
On randomly  ordered ring: latency=      0.001065 msec, bandwidth=   2316.730042 MB/s

------------------------------------------------------------------

Benchmark conditions:
 The latency   measurements were done with        8 bytes
 The bandwidth measurements were done with  2000000 bytes
 The ring communication was done in both directions on 8 processes
 The Ping Pong measurements were done on 
  -          56 pairs of processes for latency benchmarking, and 
  -          56 pairs of processes for bandwidth benchmarking, 
 out of 8*(8-1) =         56 possible combinations on 8 processes.
 (1 MB/s = 10**6 byte/sec)

------------------------------------------------------------------
Current time (1527731443) is Wed May 30 22:50:43 2018

End of LatencyBandwidth section.
Begin of HPL section.
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       2 
Q      :       4 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11R6C8       46080   512     2     4              53.85              1.211e+03
--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0033815 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
Current time (1527731506) is Wed May 30 22:51:46 2018

End of HPL section.
Begin of Summary section.
VersionMajor=1
VersionMinor=5
VersionMicro=0
VersionRelease=f
LANG=C
Success=1
sizeof_char=1
sizeof_short=2
sizeof_int=4
sizeof_long=8
sizeof_void_ptr=8
sizeof_size_t=8
sizeof_float=4
sizeof_double=8
sizeof_s64Int=8
sizeof_u64Int=8
sizeof_struct_double_double=16
CommWorldProcs=8
MPI_Wtick=1.000000e-06
HPL_Tflops=1.21132
HPL_time=53.8526
HPL_eps=1.11022e-16
HPL_RnormI=6.57494e-10
HPL_Anorm1=11637.5
HPL_AnormI=11641.7
HPL_Xnorm1=29866.1
HPL_XnormI=3.26464
HPL_BnormI=0.499991
HPL_N=46080
HPL_NB=512
HPL_nprow=2
HPL_npcol=4
HPL_depth=1
HPL_nbdiv=6
HPL_nbmin=8
HPL_cpfact=C
HPL_crfact=R
HPL_ctop=1
HPL_order=R
HPL_dMACH_EPS=1.110223e-16
HPL_dMACH_SFMIN=2.225074e-308
HPL_dMACH_BASE=2.000000e+00
HPL_dMACH_PREC=2.220446e-16
HPL_dMACH_MLEN=5.300000e+01
HPL_dMACH_RND=1.000000e+00
HPL_dMACH_EMIN=-1.021000e+03
HPL_dMACH_RMIN=2.225074e-308
HPL_dMACH_EMAX=1.024000e+03
HPL_dMACH_RMAX=1.797693e+308
HPL_sMACH_EPS=5.960464e-08
HPL_sMACH_SFMIN=1.175494e-38
HPL_sMACH_BASE=2.000000e+00
HPL_sMACH_PREC=1.192093e-07
HPL_sMACH_MLEN=2.400000e+01
HPL_sMACH_RND=1.000000e+00
HPL_sMACH_EMIN=-1.250000e+02
HPL_sMACH_RMIN=1.175494e-38
HPL_sMACH_EMAX=1.280000e+02
HPL_sMACH_RMAX=3.402823e+38
dweps=1.110223e-16
sweps=5.960464e-08
HPLMaxProcs=8
HPLMinProcs=8
DGEMM_N=9405
StarDGEMM_Gflops=189.136
SingleDGEMM_Gflops=206.278
PTRANS_GBs=4.65832
PTRANS_time=0.809135
PTRANS_residual=0
PTRANS_n=23040
PTRANS_nb=512
PTRANS_nprow=2
PTRANS_npcol=4
MPIRandomAccess_LCG_N=1073741824
MPIRandomAccess_LCG_time=60.0133
MPIRandomAccess_LCG_CheckTime=2.89138
MPIRandomAccess_LCG_Errors=0
MPIRandomAccess_LCG_ErrorsFraction=0
MPIRandomAccess_LCG_ExeUpdates=996373776
MPIRandomAccess_LCG_GUPs=0.0166025
MPIRandomAccess_LCG_TimeBound=60
MPIRandomAccess_LCG_Algorithm=0
MPIRandomAccess_N=1073741824
MPIRandomAccess_time=59.5969
MPIRandomAccess_CheckTime=2.78124
MPIRandomAccess_Errors=0
MPIRandomAccess_ErrorsFraction=0
MPIRandomAccess_ExeUpdates=925359288
MPIRandomAccess_GUPs=0.015527
MPIRandomAccess_TimeBound=60
MPIRandomAccess_Algorithm=0
RandomAccess_LCG_N=134217728
StarRandomAccess_LCG_GUPs=0.0910053
SingleRandomAccess_LCG_GUPs=0.0829739
RandomAccess_N=134217728
StarRandomAccess_GUPs=0.0888941
SingleRandomAccess_GUPs=0.0812454
STREAM_VectorSize=88473600
STREAM_Threads=8
StarSTREAM_Copy=5.80826
StarSTREAM_Scale=13.354
StarSTREAM_Add=13.0326
StarSTREAM_Triad=13.0008
SingleSTREAM_Copy=38.445
SingleSTREAM_Scale=29.7476
SingleSTREAM_Add=34.56
SingleSTREAM_Triad=34.5229
FFT_N=33554432
StarFFT_Gflops=8.09956
SingleFFT_Gflops=13.5862
MPIFFT_N=134217728
MPIFFT_Gflops=10.4534
MPIFFT_maxErr=4.25973e-15
MPIFFT_Procs=8
MaxPingPongLatency_usec=1.45038
RandomlyOrderedRingLatency_usec=1.06549
MinPingPongBandwidth_GBytes=3.70685
NaturallyOrderedRingBandwidth_GBytes=3.42637
RandomlyOrderedRingBandwidth_GBytes=2.31673
MinPingPongLatency_usec=0.536442
AvgPingPongLatency_usec=1.01452
MaxPingPongBandwidth_GBytes=15.0333
AvgPingPongBandwidth_GBytes=8.03075
NaturallyOrderedRingLatency_usec=1.00136
FFTEnblk=16
FFTEnp=8
FFTEl2size=1048576
M_OPENMP=201511
omp_get_num_threads=8
omp_get_max_threads=8
omp_get_num_procs=8
MemProc=-1
MemSpec=-1
MemVal=-1
MPIFFT_time0=1.90735e-06
MPIFFT_time1=0.322022
MPIFFT_time2=0.0612512
MPIFFT_time3=0.444471
MPIFFT_time4=0.199995
MPIFFT_time5=0.664041
MPIFFT_time6=9.53674e-07
CPS_HPCC_FFT_235=0
CPS_HPCC_FFTW_ESTIMATE=0
CPS_HPCC_MEMALLCTR=0
CPS_HPL_USE_GETPROCESSTIMES=0
CPS_RA_SANDIA_NOPT=0
CPS_RA_SANDIA_OPT2=0
CPS_USING_FFTW=0
End of Summary section.
########################################################################
End of HPC Challenge tests.
Current time (1527731506) is Wed May 30 22:51:46 2018

########################################################################
