########################################################################
This is the DARPA/DOE HPC Challenge Benchmark version 1.5.0 October 2012
Produced by Jack Dongarra and Piotr Luszczek
Innovative Computing Laboratory
University of Tennessee Knoxville and Oak Ridge National Laboratory

See the source files for authors of specific codes.
Compiled on May 30 2018 at 21:47:54
Current time (1527729086) is Wed May 30 22:11:26 2018

Hostname: 'hpc1-ib'
########################################################################
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       1 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Begin of MPIRandomAccess section.
Running on 8 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^27 = 134217728 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 866796032 (for a TIME BOUND of 60.00 secs)
CPU time used = 44.117036 seconds
Real time used = 59.274329 seconds
0.014623464 Billion(10^9) Updates    per second [GUP/s]
0.001827933 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 2.554398 seconds
Verification:  Real time used = 2.555474 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527729151) is Wed May 30 22:12:31 2018

End of MPIRandomAccess section.
Begin of StarRandomAccess section.
Main table size   = 2^27 = 134217728 words
Number of updates = 536870912
CPU time used  = 44.447631 seconds
Real time used = 5.571833 seconds
0.096354449 Billion(10^9) Updates    per second [GUP/s]
Found 3505 errors in 134217728 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.080876
Average GUP/s 0.089867
Maximum GUP/s 0.101306
Current time (1527729163) is Wed May 30 22:12:43 2018

End of StarRandomAccess section.
Begin of SingleRandomAccess section.
Node(s) with error 0
Node selected 6
Single GUP/s 0.081285
Current time (1527729176) is Wed May 30 22:12:56 2018

End of SingleRandomAccess section.
Begin of MPIRandomAccess_LCG section.
Running on 8 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^27 = 134217728 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 866239528 (for a TIME BOUND of 60.00 secs)
CPU time used = 44.057976 seconds
Real time used = 59.441935 seconds
0.014572869 Billion(10^9) Updates    per second [GUP/s]
0.001821609 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 2.533514 seconds
Verification:  Real time used = 2.533553 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527729241) is Wed May 30 22:14:01 2018

End of MPIRandomAccess_LCG section.
Begin of StarRandomAccess_LCG section.
Main table size   = 2^27 = 134217728 words
Number of updates = 536870912
CPU time used  = 44.801545 seconds
Real time used = 5.606838 seconds
0.095752885 Billion(10^9) Updates    per second [GUP/s]
Found 28 errors in 134217728 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.082952
Average GUP/s 0.090127
Maximum GUP/s 0.098740
Current time (1527729253) is Wed May 30 22:14:13 2018

End of StarRandomAccess_LCG section.
Begin of SingleRandomAccess_LCG section.
Node(s) with error 0
Node selected 3
Single GUP/s 0.097021
Current time (1527729265) is Wed May 30 22:14:25 2018

End of SingleRandomAccess_LCG section.
Begin of PTRANS section.
M: 23040
N: 23040
MB: 512
NB: 512
P: 1
Q: 8
TIME   M     N    MB  NB  P   Q     TIME   CHECK   GB/s   RESID
---- ----- ----- --- --- --- --- -------- ------ -------- -----
WALL 23040 23040 512 512   1   8     0.78 PASSED    5.463  0.00
CPU  23040 23040 512 512   1   8     0.69 PASSED    6.150  0.00
WALL 23040 23040 512 512   1   8     0.77 PASSED    5.463  0.00
CPU  23040 23040 512 512   1   8     0.69 PASSED    6.181  0.00
WALL 23040 23040 512 512   1   8     0.78 PASSED    5.444  0.00
CPU  23040 23040 512 512   1   8     0.70 PASSED    6.110  0.00
WALL 23040 23040 512 512   1   8     0.81 PASSED    5.272  0.00
CPU  23040 23040 512 512   1   8     0.72 PASSED    5.883  0.00
WALL 23040 23040 512 512   1   8     0.78 PASSED    5.272  0.00
CPU  23040 23040 512 512   1   8     0.69 PASSED    6.117  0.00

Finished    5 tests, with the following results:
    5 tests completed and passed residual checks.
    0 tests completed and failed residual checks.
    0 tests skipped because of illegal input values.

END OF TESTS.
Current time (1527729291) is Wed May 30 22:14:51 2018

End of PTRANS section.
Begin of StarDGEMM section.
Scaled residual: 0.00738978
Node(s) with error 0
Minimum Gflop/s 184.677199
Average Gflop/s 190.330295
Maximum Gflop/s 195.965505
Current time (1527729308) is Wed May 30 22:15:08 2018

End of StarDGEMM section.
Begin of SingleDGEMM section.
Node(s) with error 0
Node selected 1
Single DGEMM Gflop/s 206.541009
Current time (1527729323) is Wed May 30 22:15:23 2018

End of SingleDGEMM section.
Begin of StarSTREAM section.
-------------------------------------------------------------
This system uses 8 bytes per DOUBLE PRECISION word.
-------------------------------------------------------------
Array size = 88473600, Offset = 0
Total memory required = 1.9775 GiB.
Each test is run 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
The SCALAR value used for this run is 0.420000
-------------------------------------------------------------
Number of Threads requested = 8
-------------------------------------------------------------
Your clock granularity/precision appears to be 1 microseconds.
Each test below will take on the order of 34088 microseconds.
   (= 34088 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
VERBOSE: total setup time for rank 0 = 0.217403 seconds
-------------------------------------------------------------
Function      Rate (GB/s)   Avg time     Min time     Max time
Copy:           5.7958       0.2445       0.2442       0.2456
Scale:         13.4034       0.1059       0.1056       0.1066
Add:           13.0002       0.1637       0.1633       0.1647
Triad:         13.0056       0.1633       0.1633       0.1636
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays
-------------------------------------------------------------
Node(s) with error 0
Minimum Copy GB/s 5.795800
Average Copy GB/s 5.795800
Maximum Copy GB/s 5.795800
Minimum Scale GB/s 13.403442
Average Scale GB/s 13.403442
Maximum Scale GB/s 13.403442
Minimum Add GB/s 13.000158
Average Add GB/s 13.000158
Maximum Add GB/s 13.000158
Minimum Triad GB/s 13.005568
Average Triad GB/s 13.005568
Maximum Triad GB/s 13.005568
Current time (1527729331) is Wed May 30 22:15:31 2018

End of StarSTREAM section.
Begin of SingleSTREAM section.
Node(s) with error 0
Node selected 3
Single STREAM Copy GB/s 38.187800
Single STREAM Scale GB/s 30.965859
Single STREAM Add GB/s 34.276560
Single STREAM Triad GB/s 34.804190
Current time (1527729333) is Wed May 30 22:15:33 2018

End of SingleSTREAM section.
Begin of MPIFFT section.
Number of nodes: 8
Vector size:            134217728
Generation time:     0.482
Tuning:     0.028
Computing:     1.669
Inverse FFT:     1.775
max(|x-x0|): 4.707e-15
Gflop/s:    10.855
Current time (1527729338) is Wed May 30 22:15:38 2018

End of MPIFFT section.
Begin of StarFFT section.
Vector size: 33554432
Generation time:     0.961
Tuning:     0.000
Computing:     0.319
Inverse FFT:     0.423
max(|x-x0|): 4.721e-15
Node(s) with error 0
Minimum Gflop/s 2.376761
Average Gflop/s 7.842333
Maximum Gflop/s 13.605986
Current time (1527729343) is Wed May 30 22:15:43 2018

End of StarFFT section.
Begin of SingleFFT section.
Node(s) with error 0
Node selected 2
Single FFT Gflop/s 13.106832
Current time (1527729346) is Wed May 30 22:15:46 2018

End of SingleFFT section.
Begin of LatencyBandwidth section.

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Details - level 2
-----------------

MPI_Wtime granularity.
Max. MPI_Wtick is 0.000001 sec
wtick is set to   0.000001 sec  

Message Length: 8
Latency   min / avg / max:   0.001118 /   0.001118 /   0.001118 msecs
Bandwidth min / avg / max:      7.158 /      7.158 /      7.158 MByte/s

MPI_Wtime granularity is ok.
message size:                                  8
max time :                             10.000000 secs
latency for msg:                        0.001118 msecs
estimation for ping pong:               0.100583 msecs
max number of ping pong pairs       =      99420
max client pings = max server pongs =        315
stride for latency                  =          1
Message Length: 8
Latency   min / avg / max:   0.000497 /   0.001009 /   0.001460 msecs
Bandwidth min / avg / max:      5.478 /      8.659 /     16.106 MByte/s

Message Length: 2000000
Latency   min / avg / max:   0.522494 /   0.522494 /   0.522494 msecs
Bandwidth min / avg / max:   3827.793 /   3827.793 /   3827.793 MByte/s

MPI_Wtime granularity is ok.
message size:                            2000000
max time :                             30.000000 secs
latency for msg:                        0.522494 msecs
estimation for ping pong:               4.179955 msecs
max number of ping pong pairs       =       7177
max client pings = max server pongs =         84
stride for latency                  =          1
Message Length: 2000000
Latency   min / avg / max:   0.133991 /   0.366345 /   0.543952 msecs
Bandwidth min / avg / max:   3676.795 /   8057.573 /  14926.349 MByte/s

Message Size:                           8 Byte
Natural Order Latency:           0.001001 msec
Natural Order Bandwidth:         7.989150 MB/s
Avg Random Order Latency:        0.001064 msec
Avg Random Order Bandwidth:      7.515539 MB/s

Message Size:                     2000000 Byte
Natural Order Latency:           0.618994 msec
Natural Order Bandwidth:      3231.047857 MB/s
Avg Random Order Latency:        1.013105 msec
Avg Random Order Bandwidth:   1974.128478 MB/s

Execution time (wall clock)      =     0.643 sec on 8 processes
 - for cross ping_pong latency   =     0.009 sec
 - for cross ping_pong bandwidth =     0.195 sec
 - for ring latency              =     0.011 sec
 - for ring bandwidth            =     0.428 sec

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Major Benchmark results:
------------------------

Max Ping Pong Latency:                 0.001460 msecs
Randomly Ordered Ring Latency:         0.001064 msecs
Min Ping Pong Bandwidth:            3676.795091 MB/s
Naturally Ordered Ring Bandwidth:   3231.047857 MB/s
Randomly  Ordered Ring Bandwidth:   1974.128478 MB/s

------------------------------------------------------------------

Detailed benchmark results:
Ping Pong:
Latency   min / avg / max:   0.000497 /   0.001009 /   0.001460 msecs
Bandwidth min / avg / max:   3676.795 /   8057.573 /  14926.349 MByte/s
Ring:
On naturally ordered ring: latency=      0.001001 msec, bandwidth=   3231.047857 MB/s
On randomly  ordered ring: latency=      0.001064 msec, bandwidth=   1974.128478 MB/s

------------------------------------------------------------------

Benchmark conditions:
 The latency   measurements were done with        8 bytes
 The bandwidth measurements were done with  2000000 bytes
 The ring communication was done in both directions on 8 processes
 The Ping Pong measurements were done on 
  -          56 pairs of processes for latency benchmarking, and 
  -          56 pairs of processes for bandwidth benchmarking, 
 out of 8*(8-1) =         56 possible combinations on 8 processes.
 (1 MB/s = 10**6 byte/sec)

------------------------------------------------------------------
Current time (1527729347) is Wed May 30 22:15:47 2018

End of LatencyBandwidth section.
Begin of HPL section.
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       1 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11R6C8       46080   512     1     8             105.00              6.213e+02
--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0022845 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
Current time (1527729462) is Wed May 30 22:17:42 2018

End of HPL section.
Begin of Summary section.
VersionMajor=1
VersionMinor=5
VersionMicro=0
VersionRelease=f
LANG=C
Success=1
sizeof_char=1
sizeof_short=2
sizeof_int=4
sizeof_long=8
sizeof_void_ptr=8
sizeof_size_t=8
sizeof_float=4
sizeof_double=8
sizeof_s64Int=8
sizeof_u64Int=8
sizeof_struct_double_double=16
CommWorldProcs=8
MPI_Wtick=1.000000e-06
HPL_Tflops=0.621258
HPL_time=105.002
HPL_eps=1.11022e-16
HPL_RnormI=4.44192e-10
HPL_Anorm1=11637.5
HPL_AnormI=11641.7
HPL_Xnorm1=29866.1
HPL_XnormI=3.26464
HPL_BnormI=0.499991
HPL_N=46080
HPL_NB=512
HPL_nprow=1
HPL_npcol=8
HPL_depth=1
HPL_nbdiv=6
HPL_nbmin=8
HPL_cpfact=C
HPL_crfact=R
HPL_ctop=1
HPL_order=R
HPL_dMACH_EPS=1.110223e-16
HPL_dMACH_SFMIN=2.225074e-308
HPL_dMACH_BASE=2.000000e+00
HPL_dMACH_PREC=2.220446e-16
HPL_dMACH_MLEN=5.300000e+01
HPL_dMACH_RND=1.000000e+00
HPL_dMACH_EMIN=-1.021000e+03
HPL_dMACH_RMIN=2.225074e-308
HPL_dMACH_EMAX=1.024000e+03
HPL_dMACH_RMAX=1.797693e+308
HPL_sMACH_EPS=5.960464e-08
HPL_sMACH_SFMIN=1.175494e-38
HPL_sMACH_BASE=2.000000e+00
HPL_sMACH_PREC=1.192093e-07
HPL_sMACH_MLEN=2.400000e+01
HPL_sMACH_RND=1.000000e+00
HPL_sMACH_EMIN=-1.250000e+02
HPL_sMACH_RMIN=1.175494e-38
HPL_sMACH_EMAX=1.280000e+02
HPL_sMACH_RMAX=3.402823e+38
dweps=1.110223e-16
sweps=5.960464e-08
HPLMaxProcs=8
HPLMinProcs=8
DGEMM_N=9405
StarDGEMM_Gflops=190.33
SingleDGEMM_Gflops=206.541
PTRANS_GBs=5.27191
PTRANS_time=0.781346
PTRANS_residual=0
PTRANS_n=23040
PTRANS_nb=512
PTRANS_nprow=1
PTRANS_npcol=8
MPIRandomAccess_LCG_N=1073741824
MPIRandomAccess_LCG_time=59.4419
MPIRandomAccess_LCG_CheckTime=2.53355
MPIRandomAccess_LCG_Errors=0
MPIRandomAccess_LCG_ErrorsFraction=0
MPIRandomAccess_LCG_ExeUpdates=866239528
MPIRandomAccess_LCG_GUPs=0.0145729
MPIRandomAccess_LCG_TimeBound=60
MPIRandomAccess_LCG_Algorithm=0
MPIRandomAccess_N=1073741824
MPIRandomAccess_time=59.2743
MPIRandomAccess_CheckTime=2.55547
MPIRandomAccess_Errors=0
MPIRandomAccess_ErrorsFraction=0
MPIRandomAccess_ExeUpdates=866796032
MPIRandomAccess_GUPs=0.0146235
MPIRandomAccess_TimeBound=60
MPIRandomAccess_Algorithm=0
RandomAccess_LCG_N=134217728
StarRandomAccess_LCG_GUPs=0.0901266
SingleRandomAccess_LCG_GUPs=0.0970208
RandomAccess_N=134217728
StarRandomAccess_GUPs=0.0898666
SingleRandomAccess_GUPs=0.0812849
STREAM_VectorSize=88473600
STREAM_Threads=8
StarSTREAM_Copy=5.7958
StarSTREAM_Scale=13.4034
StarSTREAM_Add=13.0002
StarSTREAM_Triad=13.0056
SingleSTREAM_Copy=38.1878
SingleSTREAM_Scale=30.9659
SingleSTREAM_Add=34.2766
SingleSTREAM_Triad=34.8042
FFT_N=33554432
StarFFT_Gflops=7.84233
SingleFFT_Gflops=13.1068
MPIFFT_N=134217728
MPIFFT_Gflops=10.8546
MPIFFT_maxErr=4.70749e-15
MPIFFT_Procs=8
MaxPingPongLatency_usec=1.46031
RandomlyOrderedRingLatency_usec=1.06446
MinPingPongBandwidth_GBytes=3.6768
NaturallyOrderedRingBandwidth_GBytes=3.23105
RandomlyOrderedRingBandwidth_GBytes=1.97413
MinPingPongLatency_usec=0.496705
AvgPingPongLatency_usec=1.00946
MaxPingPongBandwidth_GBytes=14.9263
AvgPingPongBandwidth_GBytes=8.05757
NaturallyOrderedRingLatency_usec=1.00136
FFTEnblk=16
FFTEnp=8
FFTEl2size=1048576
M_OPENMP=201611
omp_get_num_threads=8
omp_get_max_threads=8
omp_get_num_procs=8
MemProc=-1
MemSpec=-1
MemVal=-1
MPIFFT_time0=4.76837e-06
MPIFFT_time1=0.26537
MPIFFT_time2=0.063808
MPIFFT_time3=0.440373
MPIFFT_time4=0.202505
MPIFFT_time5=0.655924
MPIFFT_time6=1.90735e-06
CPS_HPCC_FFT_235=0
CPS_HPCC_FFTW_ESTIMATE=0
CPS_HPCC_MEMALLCTR=0
CPS_HPL_USE_GETPROCESSTIMES=0
CPS_RA_SANDIA_NOPT=0
CPS_RA_SANDIA_OPT2=0
CPS_USING_FFTW=0
End of Summary section.
########################################################################
End of HPC Challenge tests.
Current time (1527729462) is Wed May 30 22:17:42 2018

########################################################################
