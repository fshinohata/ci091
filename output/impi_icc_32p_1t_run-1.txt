########################################################################
This is the DARPA/DOE HPC Challenge Benchmark version 1.5.0 October 2012
Produced by Jack Dongarra and Piotr Luszczek
Innovative Computing Laboratory
University of Tennessee Knoxville and Oak Ridge National Laboratory

See the source files for authors of specific codes.
Compiled on May 30 2018 at 13:02:21
Current time (1527696145) is Wed May 30 13:02:25 2018

Hostname: 'hpc1-ib'
########################################################################
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       4 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Begin of MPIRandomAccess section.
Running on 32 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^25 = 33554432 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 270469120 (for a TIME BOUND of 60.00 secs)
CPU time used = 58.746398 seconds
Real time used = 60.036349 seconds
0.004505089 Billion(10^9) Updates    per second [GUP/s]
0.000140784 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 0.474704 seconds
Verification:  Real time used = 0.477967 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527696216) is Wed May 30 13:03:36 2018

End of MPIRandomAccess section.
Begin of StarRandomAccess section.
Main table size   = 2^25 = 33554432 words
Number of updates = 134217728
CPU time used  = 4.065211 seconds
Real time used = 4.066006 seconds
0.033009723 Billion(10^9) Updates    per second [GUP/s]
Found 0 errors in 33554432 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.032596
Average GUP/s 0.034123
Maximum GUP/s 0.035947
Current time (1527696224) is Wed May 30 13:03:44 2018

End of StarRandomAccess section.
Begin of SingleRandomAccess section.
Node(s) with error 0
Node selected 18
Single GUP/s 0.084289
Current time (1527696227) is Wed May 30 13:03:47 2018

End of SingleRandomAccess section.
Begin of MPIRandomAccess_LCG section.
Running on 32 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^25 = 33554432 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 270534688 (for a TIME BOUND of 60.00 secs)
CPU time used = 58.705294 seconds
Real time used = 59.999631 seconds
0.004508939 Billion(10^9) Updates    per second [GUP/s]
0.000140904 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 0.461703 seconds
Verification:  Real time used = 0.465767 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527696297) is Wed May 30 13:04:57 2018

End of MPIRandomAccess_LCG section.
Begin of StarRandomAccess_LCG section.
Main table size   = 2^25 = 33554432 words
Number of updates = 134217728
CPU time used  = 4.144064 seconds
Real time used = 4.144862 seconds
0.032381711 Billion(10^9) Updates    per second [GUP/s]
Found 0 errors in 33554432 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.032045
Average GUP/s 0.033894
Maximum GUP/s 0.035787
Current time (1527696306) is Wed May 30 13:05:06 2018

End of StarRandomAccess_LCG section.
Begin of SingleRandomAccess_LCG section.
Node(s) with error 0
Node selected 15
Single GUP/s 0.081771
Current time (1527696309) is Wed May 30 13:05:09 2018

End of SingleRandomAccess_LCG section.
Begin of PTRANS section.
M: 23040
N: 23040
MB: 512
NB: 512
P: 4
Q: 8
TIME   M     N    MB  NB  P   Q     TIME   CHECK   GB/s   RESID
---- ----- ----- --- --- --- --- -------- ------ -------- -----
WALL 23040 23040 512 512   4   8     0.33 PASSED   12.973  0.00
CPU  23040 23040 512 512   4   8     0.25 PASSED   17.214  0.00
WALL 23040 23040 512 512   4   8     0.37 PASSED   11.456  0.00
CPU  23040 23040 512 512   4   8     0.26 PASSED   16.167  0.00
WALL 23040 23040 512 512   4   8     0.37 PASSED   11.456  0.00
CPU  23040 23040 512 512   4   8     0.28 PASSED   15.316  0.00
WALL 23040 23040 512 512   4   8     0.36 PASSED   11.456  0.00
CPU  23040 23040 512 512   4   8     0.27 PASSED   15.846  0.00
WALL 23040 23040 512 512   4   8     0.35 PASSED   11.456  0.00
CPU  23040 23040 512 512   4   8     0.27 PASSED   15.685  0.00

Finished    5 tests, with the following results:
    5 tests completed and passed residual checks.
    0 tests completed and failed residual checks.
    0 tests skipped because of illegal input values.

END OF TESTS.
Current time (1527696323) is Wed May 30 13:05:23 2018

End of PTRANS section.
Begin of StarDGEMM section.
Scaled residual: 0.00985626
Node(s) with error 0
Minimum Gflop/s 24.075831
Average Gflop/s 24.183853
Maximum Gflop/s 24.236492
Current time (1527696333) is Wed May 30 13:05:33 2018

End of StarDGEMM section.
Begin of SingleDGEMM section.
Node(s) with error 0
Node selected 20
Single DGEMM Gflop/s 26.247098
Current time (1527696342) is Wed May 30 13:05:42 2018

End of SingleDGEMM section.
Begin of StarSTREAM section.
-------------------------------------------------------------
This system uses 8 bytes per DOUBLE PRECISION word.
-------------------------------------------------------------
Array size = 22118400, Offset = 0
Total memory required = 0.4944 GiB.
Each test is run 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
The SCALAR value used for this run is 0.420000
-------------------------------------------------------------
Number of Threads requested = 1
-------------------------------------------------------------
Your clock granularity/precision appears to be 1 microseconds.
Each test below will take on the order of 66565 microseconds.
   (= 66565 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
VERBOSE: total setup time for rank 0 = 0.402983 seconds
-------------------------------------------------------------
Function      Rate (GB/s)   Avg time     Min time     Max time
Copy:           4.1716       0.0867       0.0848       0.0988
Scale:          3.6197       0.0986       0.0978       0.0994
Add:            4.2889       0.1249       0.1238       0.1287
Triad:          4.3053       0.1238       0.1233       0.1241
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays
-------------------------------------------------------------
Node(s) with error 0
Minimum Copy GB/s 4.171559
Average Copy GB/s 4.171559
Maximum Copy GB/s 4.171559
Minimum Scale GB/s 3.619663
Average Scale GB/s 3.619663
Maximum Scale GB/s 3.619663
Minimum Add GB/s 4.288936
Average Add GB/s 4.288936
Maximum Add GB/s 4.288936
Minimum Triad GB/s 4.305324
Average Triad GB/s 4.305324
Maximum Triad GB/s 4.305324
Current time (1527696347) is Wed May 30 13:05:47 2018

End of StarSTREAM section.
Begin of SingleSTREAM section.
Node(s) with error 0
Node selected 5
Single STREAM Copy GB/s 5.808393
Single STREAM Scale GB/s 13.104331
Single STREAM Add GB/s 13.217245
Single STREAM Triad GB/s 13.152286
Current time (1527696349) is Wed May 30 13:05:49 2018

End of SingleSTREAM section.
Begin of MPIFFT section.
Number of nodes: 32
Vector size:            134217728
Generation time:     0.125
Tuning:     0.052
Computing:     0.920
Inverse FFT:     0.981
max(|x-x0|): 4.707e-15
Gflop/s:    19.692
Current time (1527696351) is Wed May 30 13:05:51 2018

End of MPIFFT section.
Begin of StarFFT section.
Vector size: 8388608
Generation time:     0.257
Tuning:     0.000
Computing:     0.522
Inverse FFT:     0.562
max(|x-x0|): 4.120e-15
Node(s) with error 0
Minimum Gflop/s 1.832197
Average Gflop/s 1.968329
Maximum Gflop/s 2.160496
Current time (1527696353) is Wed May 30 13:05:53 2018

End of StarFFT section.
Begin of SingleFFT section.
Node(s) with error 0
Node selected 26
Single FFT Gflop/s 2.734599
Current time (1527696354) is Wed May 30 13:05:54 2018

End of SingleFFT section.
Begin of LatencyBandwidth section.

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Details - level 2
-----------------

MPI_Wtime granularity.
Max. MPI_Wtick is 0.000002 sec
wtick is set to   0.000002 sec  

Message Length: 8
Latency   min / avg / max:   0.000556 /   0.000556 /   0.000556 msecs
Bandwidth min / avg / max:     14.380 /     14.380 /     14.380 MByte/s

Use MPI_Wtick for estimation of max pairs
message size:                                  8
max time :                             10.000000 secs
latency for msg:                        0.000556 msecs
estimation for ping pong:               0.050068 msecs
max number of ping pong pairs       =     100000
max client pings = max server pongs =        316
stride for latency                  =          1
Message Length: 8
Latency   min / avg / max:   0.000209 /   0.000616 /   0.000950 msecs
Bandwidth min / avg / max:      8.424 /     16.365 /     38.348 MByte/s

Message Length: 2000000
Latency   min / avg / max:   0.109553 /   0.109553 /   0.109553 msecs
Bandwidth min / avg / max:  18255.948 /  18255.948 /  18255.948 MByte/s

MPI_Wtime granularity is ok.
message size:                            2000000
max time :                             30.000000 secs
latency for msg:                        0.109553 msecs
estimation for ping pong:               0.876427 msecs
max number of ping pong pairs       =      34229
max client pings = max server pongs =        185
stride for latency                  =          1
Message Length: 2000000
Latency   min / avg / max:   0.108957 /   0.112131 /   0.120044 msecs
Bandwidth min / avg / max:  16660.592 /  17840.186 /  18355.816 MByte/s

Message Size:                           8 Byte
Natural Order Latency:           0.001095 msec
Natural Order Bandwidth:         7.308886 MB/s
Avg Random Order Latency:        0.001294 msec
Avg Random Order Bandwidth:      6.182128 MB/s

Message Size:                     2000000 Byte
Natural Order Latency:           1.214743 msec
Natural Order Bandwidth:      1646.439254 MB/s
Avg Random Order Latency:        1.649278 msec
Avg Random Order Bandwidth:   1212.651860 MB/s

Execution time (wall clock)      =     2.681 sec on 32 processes
 - for cross ping_pong latency   =     0.399 sec
 - for cross ping_pong bandwidth =     1.460 sec
 - for ring latency              =     0.034 sec
 - for ring bandwidth            =     0.787 sec

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Major Benchmark results:
------------------------

Max Ping Pong Latency:                 0.000950 msecs
Randomly Ordered Ring Latency:         0.001294 msecs
Min Ping Pong Bandwidth:           16660.591857 MB/s
Naturally Ordered Ring Bandwidth:   1646.439254 MB/s
Randomly  Ordered Ring Bandwidth:   1212.651860 MB/s

------------------------------------------------------------------

Detailed benchmark results:
Ping Pong:
Latency   min / avg / max:   0.000209 /   0.000616 /   0.000950 msecs
Bandwidth min / avg / max:  16660.592 /  17840.186 /  18355.816 MByte/s
Ring:
On naturally ordered ring: latency=      0.001095 msec, bandwidth=   1646.439254 MB/s
On randomly  ordered ring: latency=      0.001294 msec, bandwidth=   1212.651860 MB/s

------------------------------------------------------------------

Benchmark conditions:
 The latency   measurements were done with        8 bytes
 The bandwidth measurements were done with  2000000 bytes
 The ring communication was done in both directions on 32 processes
 The Ping Pong measurements were done on 
  -         992 pairs of processes for latency benchmarking, and 
  -         992 pairs of processes for bandwidth benchmarking, 
 out of 32*(32-1) =        992 possible combinations on 32 processes.
 (1 MB/s = 10**6 byte/sec)

------------------------------------------------------------------
Current time (1527696357) is Wed May 30 13:05:57 2018

End of LatencyBandwidth section.
Begin of HPL section.
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       4 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11R6C8       46080   512     4     8              99.75              6.540e+02
--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0029546 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
Current time (1527696460) is Wed May 30 13:07:40 2018

End of HPL section.
Begin of Summary section.
VersionMajor=1
VersionMinor=5
VersionMicro=0
VersionRelease=f
LANG=C
Success=1
sizeof_char=1
sizeof_short=2
sizeof_int=4
sizeof_long=8
sizeof_void_ptr=8
sizeof_size_t=8
sizeof_float=4
sizeof_double=8
sizeof_s64Int=8
sizeof_u64Int=8
sizeof_struct_double_double=16
CommWorldProcs=32
MPI_Wtick=2.000000e-06
HPL_Tflops=0.653992
HPL_time=99.7459
HPL_eps=1.11022e-16
HPL_RnormI=5.74488e-10
HPL_Anorm1=11637.5
HPL_AnormI=11641.7
HPL_Xnorm1=29866.1
HPL_XnormI=3.26464
HPL_BnormI=0.499991
HPL_N=46080
HPL_NB=512
HPL_nprow=4
HPL_npcol=8
HPL_depth=1
HPL_nbdiv=6
HPL_nbmin=8
HPL_cpfact=C
HPL_crfact=R
HPL_ctop=1
HPL_order=R
HPL_dMACH_EPS=1.110223e-16
HPL_dMACH_SFMIN=2.225074e-308
HPL_dMACH_BASE=2.000000e+00
HPL_dMACH_PREC=2.220446e-16
HPL_dMACH_MLEN=5.300000e+01
HPL_dMACH_RND=1.000000e+00
HPL_dMACH_EMIN=-1.021000e+03
HPL_dMACH_RMIN=2.225074e-308
HPL_dMACH_EMAX=1.024000e+03
HPL_dMACH_RMAX=1.797693e+308
HPL_sMACH_EPS=5.960464e-08
HPL_sMACH_SFMIN=1.175494e-38
HPL_sMACH_BASE=2.000000e+00
HPL_sMACH_PREC=1.192093e-07
HPL_sMACH_MLEN=2.400000e+01
HPL_sMACH_RND=1.000000e+00
HPL_sMACH_EMIN=-1.250000e+02
HPL_sMACH_RMIN=1.175494e-38
HPL_sMACH_EMAX=1.280000e+02
HPL_sMACH_RMAX=3.402823e+38
dweps=1.110223e-16
sweps=5.960464e-08
HPLMaxProcs=32
HPLMinProcs=32
DGEMM_N=4702
StarDGEMM_Gflops=24.1839
SingleDGEMM_Gflops=26.2471
PTRANS_GBs=11.4563
PTRANS_time=0.354418
PTRANS_residual=0
PTRANS_n=23040
PTRANS_nb=512
PTRANS_nprow=4
PTRANS_npcol=8
MPIRandomAccess_LCG_N=1073741824
MPIRandomAccess_LCG_time=59.9996
MPIRandomAccess_LCG_CheckTime=0.465767
MPIRandomAccess_LCG_Errors=0
MPIRandomAccess_LCG_ErrorsFraction=0
MPIRandomAccess_LCG_ExeUpdates=270534688
MPIRandomAccess_LCG_GUPs=0.00450894
MPIRandomAccess_LCG_TimeBound=60
MPIRandomAccess_LCG_Algorithm=0
MPIRandomAccess_N=1073741824
MPIRandomAccess_time=60.0363
MPIRandomAccess_CheckTime=0.477967
MPIRandomAccess_Errors=0
MPIRandomAccess_ErrorsFraction=0
MPIRandomAccess_ExeUpdates=270469120
MPIRandomAccess_GUPs=0.00450509
MPIRandomAccess_TimeBound=60
MPIRandomAccess_Algorithm=0
RandomAccess_LCG_N=33554432
StarRandomAccess_LCG_GUPs=0.0338935
SingleRandomAccess_LCG_GUPs=0.0817713
RandomAccess_N=33554432
StarRandomAccess_GUPs=0.0341229
SingleRandomAccess_GUPs=0.0842886
STREAM_VectorSize=22118400
STREAM_Threads=1
StarSTREAM_Copy=4.17156
StarSTREAM_Scale=3.61966
StarSTREAM_Add=4.28894
StarSTREAM_Triad=4.30532
SingleSTREAM_Copy=5.80839
SingleSTREAM_Scale=13.1043
SingleSTREAM_Add=13.2172
SingleSTREAM_Triad=13.1523
FFT_N=8388608
StarFFT_Gflops=1.96833
SingleFFT_Gflops=2.7346
MPIFFT_N=134217728
MPIFFT_Gflops=19.6915
MPIFFT_maxErr=4.70749e-15
MPIFFT_Procs=32
MaxPingPongLatency_usec=0.949701
RandomlyOrderedRingLatency_usec=1.29405
MinPingPongBandwidth_GBytes=16.6606
NaturallyOrderedRingBandwidth_GBytes=1.64644
RandomlyOrderedRingBandwidth_GBytes=1.21265
MinPingPongLatency_usec=0.208616
AvgPingPongLatency_usec=0.616257
MaxPingPongBandwidth_GBytes=18.3558
AvgPingPongBandwidth_GBytes=17.8402
NaturallyOrderedRingLatency_usec=1.09456
FFTEnblk=16
FFTEnp=8
FFTEl2size=1048576
M_OPENMP=201611
omp_get_num_threads=1
omp_get_max_threads=1
omp_get_num_procs=1
MemProc=-1
MemSpec=-1
MemVal=-1
MPIFFT_time0=5.00679e-06
MPIFFT_time1=0.129668
MPIFFT_time2=0.124647
MPIFFT_time3=0.093652
MPIFFT_time4=0.369062
MPIFFT_time5=0.166869
MPIFFT_time6=2.86102e-06
CPS_HPCC_FFT_235=0
CPS_HPCC_FFTW_ESTIMATE=0
CPS_HPCC_MEMALLCTR=0
CPS_HPL_USE_GETPROCESSTIMES=0
CPS_RA_SANDIA_NOPT=0
CPS_RA_SANDIA_OPT2=0
CPS_USING_FFTW=0
End of Summary section.
########################################################################
End of HPC Challenge tests.
Current time (1527696460) is Wed May 30 13:07:40 2018

########################################################################
