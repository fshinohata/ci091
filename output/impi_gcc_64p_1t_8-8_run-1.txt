########################################################################
This is the DARPA/DOE HPC Challenge Benchmark version 1.5.0 October 2012
Produced by Jack Dongarra and Piotr Luszczek
Innovative Computing Laboratory
University of Tennessee Knoxville and Oak Ridge National Laboratory

See the source files for authors of specific codes.
Compiled on May 30 2018 at 22:45:59
Current time (1527733368) is Wed May 30 23:22:48 2018

Hostname: 'hpc1-ib'
########################################################################
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       8 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Begin of MPIRandomAccess section.
Running on 64 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^24 = 16777216 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 1707439040 (for a TIME BOUND of 60.00 secs)
CPU time used = 29.248789 seconds
Real time used = 33.672608 seconds
0.050707063 Billion(10^9) Updates    per second [GUP/s]
0.000792298 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 2.827982 seconds
Verification:  Real time used = 2.865583 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527733406) is Wed May 30 23:23:26 2018

End of MPIRandomAccess section.
Begin of StarRandomAccess section.
Main table size   = 2^24 = 16777216 words
Number of updates = 67108864
CPU time used  = 2.031581 seconds
Real time used = 2.069725 seconds
0.032424048 Billion(10^9) Updates    per second [GUP/s]
Found 0 errors in 16777216 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.030226
Average GUP/s 0.033980
Maximum GUP/s 0.037088
Current time (1527733411) is Wed May 30 23:23:31 2018

End of StarRandomAccess section.
Begin of SingleRandomAccess section.
Node(s) with error 0
Node selected 9
Single GUP/s 0.089326
Current time (1527733412) is Wed May 30 23:23:32 2018

End of SingleRandomAccess section.
Begin of MPIRandomAccess_LCG section.
Running on 64 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^24 = 16777216 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 3025094336 (for a TIME BOUND of 60.00 secs)
CPU time used = 52.346009 seconds
Real time used = 59.898272 seconds
0.050503867 Billion(10^9) Updates    per second [GUP/s]
0.000789123 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 4.566151 seconds
Verification:  Real time used = 4.594190 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527733478) is Wed May 30 23:24:38 2018

End of MPIRandomAccess_LCG section.
Begin of StarRandomAccess_LCG section.
Main table size   = 2^24 = 16777216 words
Number of updates = 67108864
CPU time used  = 2.039774 seconds
Real time used = 2.042779 seconds
0.032851750 Billion(10^9) Updates    per second [GUP/s]
Found 0 errors in 16777216 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.029174
Average GUP/s 0.034073
Maximum GUP/s 0.037421
Current time (1527733482) is Wed May 30 23:24:42 2018

End of StarRandomAccess_LCG section.
Begin of SingleRandomAccess_LCG section.
Node(s) with error 0
Node selected 6
Single GUP/s 0.090803
Current time (1527733483) is Wed May 30 23:24:43 2018

End of SingleRandomAccess_LCG section.
Begin of PTRANS section.
M: 23040
N: 23040
MB: 512
NB: 512
P: 8
Q: 8
TIME   M     N    MB  NB  P   Q     TIME   CHECK   GB/s   RESID
---- ----- ----- --- --- --- --- -------- ------ -------- -----
WALL 23040 23040 512 512   8   8     0.61 PASSED    6.989  0.00
CPU  23040 23040 512 512   8   8     0.60 PASSED    7.136  0.00
WALL 23040 23040 512 512   8   8     0.53 PASSED    6.989  0.00
CPU  23040 23040 512 512   8   8     0.53 PASSED    8.068  0.00
WALL 23040 23040 512 512   8   8     0.55 PASSED    6.989  0.00
CPU  23040 23040 512 512   8   8     0.54 PASSED    7.877  0.00
WALL 23040 23040 512 512   8   8     0.52 PASSED    6.989  0.00
CPU  23040 23040 512 512   8   8     0.52 PASSED    8.202  0.00
WALL 23040 23040 512 512   8   8     0.50 PASSED    6.989  0.00
CPU  23040 23040 512 512   8   8     0.50 PASSED    8.529  0.00

Finished    5 tests, with the following results:
    5 tests completed and passed residual checks.
    0 tests completed and failed residual checks.
    0 tests skipped because of illegal input values.

END OF TESTS.
Current time (1527733492) is Wed May 30 23:24:52 2018

End of PTRANS section.
Begin of StarDGEMM section.
Scaled residual: 0.0105163
Node(s) with error 0
Minimum Gflop/s 19.117382
Average Gflop/s 20.906962
Maximum Gflop/s 22.607583
Current time (1527733497) is Wed May 30 23:24:57 2018

End of StarDGEMM section.
Begin of SingleDGEMM section.
Node(s) with error 0
Node selected 30
Single DGEMM Gflop/s 25.924078
Current time (1527733501) is Wed May 30 23:25:01 2018

End of SingleDGEMM section.
Begin of StarSTREAM section.
-------------------------------------------------------------
This system uses 8 bytes per DOUBLE PRECISION word.
-------------------------------------------------------------
Array size = 11059200, Offset = 0
Total memory required = 0.2472 GiB.
Each test is run 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
The SCALAR value used for this run is 0.420000
-------------------------------------------------------------
Number of Threads requested = 1
-------------------------------------------------------------
Your clock granularity/precision appears to be 1 microseconds.
Each test below will take on the order of 32213 microseconds.
   (= 32213 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
VERBOSE: total setup time for rank 0 = 0.197798 seconds
-------------------------------------------------------------
Function      Rate (GB/s)   Avg time     Min time     Max time
Copy:           4.1602       0.0463       0.0425       0.0593
Scale:          3.5931       0.0496       0.0492       0.0499
Add:            4.1360       0.0648       0.0642       0.0666
Triad:          4.1582       0.0645       0.0638       0.0650
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays
-------------------------------------------------------------
Node(s) with error 0
Minimum Copy GB/s 4.160241
Average Copy GB/s 4.160241
Maximum Copy GB/s 4.160241
Minimum Scale GB/s 3.593053
Average Scale GB/s 3.593053
Maximum Scale GB/s 3.593053
Minimum Add GB/s 4.135959
Average Add GB/s 4.135959
Maximum Add GB/s 4.135959
Minimum Triad GB/s 4.158174
Average Triad GB/s 4.158174
Maximum Triad GB/s 4.158174
Current time (1527733504) is Wed May 30 23:25:04 2018

End of StarSTREAM section.
Begin of SingleSTREAM section.
Node(s) with error 0
Node selected 31
Single STREAM Copy GB/s 5.886270
Single STREAM Scale GB/s 12.557449
Single STREAM Add GB/s 12.625810
Single STREAM Triad GB/s 12.729321
Current time (1527733505) is Wed May 30 23:25:05 2018

End of SingleSTREAM section.
Begin of MPIFFT section.
Number of nodes: 64
Vector size:            134217728
Generation time:     0.054
Tuning:     0.109
Computing:     0.795
Inverse FFT:     0.813
max(|x-x0|): 4.663e-15
Gflop/s:    22.802
Current time (1527733507) is Wed May 30 23:25:07 2018

End of MPIFFT section.
Begin of StarFFT section.
Vector size: 4194304
Generation time:     0.107
Tuning:     0.001
Computing:     0.218
Inverse FFT:     0.263
max(|x-x0|): 2.048e-15
Node(s) with error 0
Minimum Gflop/s 1.753512
Average Gflop/s 1.962458
Maximum Gflop/s 2.228706
Current time (1527733507) is Wed May 30 23:25:07 2018

End of StarFFT section.
Begin of SingleFFT section.
Node(s) with error 0
Node selected 35
Single FFT Gflop/s 2.320348
Current time (1527733508) is Wed May 30 23:25:08 2018

End of SingleFFT section.
Begin of LatencyBandwidth section.

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Details - level 2
-----------------

MPI_Wtime granularity.
Max. MPI_Wtick is 0.000001 sec
wtick is set to   0.000001 sec  

Message Length: 8
Latency   min / avg / max:   0.001118 /   0.001118 /   0.001118 msecs
Bandwidth min / avg / max:      7.158 /      7.158 /      7.158 MByte/s

MPI_Wtime granularity is ok.
message size:                                  8
max time :                             10.000000 secs
latency for msg:                        0.001118 msecs
estimation for ping pong:               0.100583 msecs
max number of ping pong pairs       =      99420
max client pings = max server pongs =        315
stride for latency                  =          1
Message Length: 8
Latency   min / avg / max:   0.000221 /   0.000947 /   0.001466 msecs
Bandwidth min / avg / max:      5.458 /     11.215 /     36.239 MByte/s

Message Length: 2000000
Latency   min / avg / max:   0.524521 /   0.524521 /   0.524521 msecs
Bandwidth min / avg / max:   3813.004 /   3813.004 /   3813.004 MByte/s

MPI_Wtime granularity is ok.
message size:                            2000000
max time :                             30.000000 secs
latency for msg:                        0.524521 msecs
estimation for ping pong:               4.196167 msecs
max number of ping pong pairs       =       7149
max client pings = max server pongs =         84
stride for latency                  =          1
Message Length: 2000000
Latency   min / avg / max:   0.105500 /   0.341650 /   0.558972 msecs
Bandwidth min / avg / max:   3577.994 /   8890.911 /  18957.306 MByte/s

Message Size:                           8 Byte
Natural Order Latency:           0.001454 msec
Natural Order Bandwidth:         5.500727 MB/s
Avg Random Order Latency:        0.005259 msec
Avg Random Order Bandwidth:      1.521283 MB/s

Message Size:                     2000000 Byte
Natural Order Latency:           1.794040 msec
Natural Order Bandwidth:      1114.802219 MB/s
Avg Random Order Latency:        6.659588 msec
Avg Random Order Bandwidth:    300.318886 MB/s

Execution time (wall clock)      =    17.050 sec on 64 processes
 - for cross ping_pong latency   =     1.235 sec
 - for cross ping_pong bandwidth =    13.288 sec
 - for ring latency              =     0.080 sec
 - for ring bandwidth            =     2.447 sec

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Major Benchmark results:
------------------------

Max Ping Pong Latency:                 0.001466 msecs
Randomly Ordered Ring Latency:         0.005259 msecs
Min Ping Pong Bandwidth:            3577.994455 MB/s
Naturally Ordered Ring Bandwidth:   1114.802219 MB/s
Randomly  Ordered Ring Bandwidth:    300.318886 MB/s

------------------------------------------------------------------

Detailed benchmark results:
Ping Pong:
Latency   min / avg / max:   0.000221 /   0.000947 /   0.001466 msecs
Bandwidth min / avg / max:   3577.994 /   8890.911 /  18957.306 MByte/s
Ring:
On naturally ordered ring: latency=      0.001454 msec, bandwidth=   1114.802219 MB/s
On randomly  ordered ring: latency=      0.005259 msec, bandwidth=    300.318886 MB/s

------------------------------------------------------------------

Benchmark conditions:
 The latency   measurements were done with        8 bytes
 The bandwidth measurements were done with  2000000 bytes
 The ring communication was done in both directions on 64 processes
 The Ping Pong measurements were done on 
  -        4032 pairs of processes for latency benchmarking, and 
  -        4032 pairs of processes for bandwidth benchmarking, 
 out of 64*(64-1) =       4032 possible combinations on 64 processes.
 (1 MB/s = 10**6 byte/sec)

------------------------------------------------------------------
Current time (1527733525) is Wed May 30 23:25:25 2018

End of LatencyBandwidth section.
Begin of HPL section.
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       8 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11R6C8       46080   512     8     8              56.15              1.162e+03
--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0030317 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
Current time (1527733583) is Wed May 30 23:26:23 2018

End of HPL section.
Begin of Summary section.
VersionMajor=1
VersionMinor=5
VersionMicro=0
VersionRelease=f
LANG=C
Success=1
sizeof_char=1
sizeof_short=2
sizeof_int=4
sizeof_long=8
sizeof_void_ptr=8
sizeof_size_t=8
sizeof_float=4
sizeof_double=8
sizeof_s64Int=8
sizeof_u64Int=8
sizeof_struct_double_double=16
CommWorldProcs=64
MPI_Wtick=1.000000e-06
HPL_Tflops=1.1618
HPL_time=56.1484
HPL_eps=1.11022e-16
HPL_RnormI=5.89474e-10
HPL_Anorm1=11637.5
HPL_AnormI=11641.7
HPL_Xnorm1=29866.1
HPL_XnormI=3.26464
HPL_BnormI=0.499991
HPL_N=46080
HPL_NB=512
HPL_nprow=8
HPL_npcol=8
HPL_depth=1
HPL_nbdiv=6
HPL_nbmin=8
HPL_cpfact=C
HPL_crfact=R
HPL_ctop=1
HPL_order=R
HPL_dMACH_EPS=1.110223e-16
HPL_dMACH_SFMIN=2.225074e-308
HPL_dMACH_BASE=2.000000e+00
HPL_dMACH_PREC=2.220446e-16
HPL_dMACH_MLEN=5.300000e+01
HPL_dMACH_RND=1.000000e+00
HPL_dMACH_EMIN=-1.021000e+03
HPL_dMACH_RMIN=2.225074e-308
HPL_dMACH_EMAX=1.024000e+03
HPL_dMACH_RMAX=1.797693e+308
HPL_sMACH_EPS=5.960464e-08
HPL_sMACH_SFMIN=1.175494e-38
HPL_sMACH_BASE=2.000000e+00
HPL_sMACH_PREC=1.192093e-07
HPL_sMACH_MLEN=2.400000e+01
HPL_sMACH_RND=1.000000e+00
HPL_sMACH_EMIN=-1.250000e+02
HPL_sMACH_RMIN=1.175494e-38
HPL_sMACH_EMAX=1.280000e+02
HPL_sMACH_RMAX=3.402823e+38
dweps=1.110223e-16
sweps=5.960464e-08
HPLMaxProcs=64
HPLMinProcs=64
DGEMM_N=3325
StarDGEMM_Gflops=20.907
SingleDGEMM_Gflops=25.9241
PTRANS_GBs=6.98945
PTRANS_time=0.503478
PTRANS_residual=0
PTRANS_n=23040
PTRANS_nb=512
PTRANS_nprow=8
PTRANS_npcol=8
MPIRandomAccess_LCG_N=1073741824
MPIRandomAccess_LCG_time=59.8983
MPIRandomAccess_LCG_CheckTime=4.59419
MPIRandomAccess_LCG_Errors=0
MPIRandomAccess_LCG_ErrorsFraction=0
MPIRandomAccess_LCG_ExeUpdates=3025094336
MPIRandomAccess_LCG_GUPs=0.0505039
MPIRandomAccess_LCG_TimeBound=60
MPIRandomAccess_LCG_Algorithm=0
MPIRandomAccess_N=1073741824
MPIRandomAccess_time=33.6726
MPIRandomAccess_CheckTime=2.86558
MPIRandomAccess_Errors=0
MPIRandomAccess_ErrorsFraction=0
MPIRandomAccess_ExeUpdates=1707439040
MPIRandomAccess_GUPs=0.0507071
MPIRandomAccess_TimeBound=60
MPIRandomAccess_Algorithm=0
RandomAccess_LCG_N=16777216
StarRandomAccess_LCG_GUPs=0.0340734
SingleRandomAccess_LCG_GUPs=0.0908031
RandomAccess_N=16777216
StarRandomAccess_GUPs=0.0339804
SingleRandomAccess_GUPs=0.0893258
STREAM_VectorSize=11059200
STREAM_Threads=1
StarSTREAM_Copy=4.16024
StarSTREAM_Scale=3.59305
StarSTREAM_Add=4.13596
StarSTREAM_Triad=4.15817
SingleSTREAM_Copy=5.88627
SingleSTREAM_Scale=12.5574
SingleSTREAM_Add=12.6258
SingleSTREAM_Triad=12.7293
FFT_N=4194304
StarFFT_Gflops=1.96246
SingleFFT_Gflops=2.32035
MPIFFT_N=134217728
MPIFFT_Gflops=22.8017
MPIFFT_maxErr=4.66311e-15
MPIFFT_Procs=64
MaxPingPongLatency_usec=1.46583
RandomlyOrderedRingLatency_usec=5.25872
MinPingPongBandwidth_GBytes=3.57799
NaturallyOrderedRingBandwidth_GBytes=1.1148
RandomlyOrderedRingBandwidth_GBytes=0.300319
MinPingPongLatency_usec=0.220758
AvgPingPongLatency_usec=0.947272
MaxPingPongBandwidth_GBytes=18.9573
AvgPingPongBandwidth_GBytes=8.89091
NaturallyOrderedRingLatency_usec=1.45435
FFTEnblk=16
FFTEnp=8
FFTEl2size=1048576
M_OPENMP=201511
omp_get_num_threads=1
omp_get_max_threads=1
omp_get_num_procs=1
MemProc=-1
MemSpec=-1
MemVal=-1
MPIFFT_time0=9.53674e-07
MPIFFT_time1=0.175672
MPIFFT_time2=0.0629609
MPIFFT_time3=0.184423
MPIFFT_time4=0.120357
MPIFFT_time5=0.234225
MPIFFT_time6=9.53674e-07
CPS_HPCC_FFT_235=0
CPS_HPCC_FFTW_ESTIMATE=0
CPS_HPCC_MEMALLCTR=0
CPS_HPL_USE_GETPROCESSTIMES=0
CPS_RA_SANDIA_NOPT=0
CPS_RA_SANDIA_OPT2=0
CPS_USING_FFTW=0
End of Summary section.
########################################################################
End of HPC Challenge tests.
Current time (1527733583) is Wed May 30 23:26:23 2018

########################################################################
