########################################################################
This is the DARPA/DOE HPC Challenge Benchmark version 1.5.0 October 2012
Produced by Jack Dongarra and Piotr Luszczek
Innovative Computing Laboratory
University of Tennessee Knoxville and Oak Ridge National Laboratory

See the source files for authors of specific codes.
Compiled on May 29 2018 at 23:36:12
Current time (1527647776) is Tue May 29 23:36:16 2018

Hostname: 'hpc1-ib'
########################################################################
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       1 
Q      :       4 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Begin of MPIRandomAccess section.
Running on 4 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^28 = 268435456 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 1626629596 (for a TIME BOUND of 60.00 secs)
CPU time used = 58.752753 seconds
Real time used = 58.768661 seconds
0.027678520 Billion(10^9) Updates    per second [GUP/s]
0.006919630 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 10.821798 seconds
Verification:  Real time used = 10.823097 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527647848) is Tue May 29 23:37:28 2018

End of MPIRandomAccess section.
Begin of StarRandomAccess section.
Main table size   = 2^28 = 268435456 words
Number of updates = 1073741824
CPU time used  = 93.391692 seconds
Real time used = 11.679387 seconds
0.091934773 Billion(10^9) Updates    per second [GUP/s]
Found 3415 errors in 268435456 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.091935
Average GUP/s 0.094194
Maximum GUP/s 0.096150
Current time (1527647872) is Tue May 29 23:37:52 2018

End of StarRandomAccess section.
Begin of SingleRandomAccess section.
Node(s) with error 0
Node selected 2
Single GUP/s 0.094581
Current time (1527647895) is Tue May 29 23:38:15 2018

End of SingleRandomAccess section.
Begin of MPIRandomAccess_LCG section.
Running on 4 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^28 = 268435456 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 1649620204 (for a TIME BOUND of 60.00 secs)
CPU time used = 58.938452 seconds
Real time used = 58.943715 seconds
0.027986363 Billion(10^9) Updates    per second [GUP/s]
0.006996591 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 10.412627 seconds
Verification:  Real time used = 10.417791 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527647967) is Tue May 29 23:39:27 2018

End of MPIRandomAccess_LCG section.
Begin of StarRandomAccess_LCG section.
Main table size   = 2^28 = 268435456 words
Number of updates = 1073741824
CPU time used  = 89.127266 seconds
Real time used = 11.144073 seconds
0.096350934 Billion(10^9) Updates    per second [GUP/s]
Found 19 errors in 268435456 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.087729
Average GUP/s 0.094812
Maximum GUP/s 0.098683
Current time (1527647991) is Tue May 29 23:39:51 2018

End of StarRandomAccess_LCG section.
Begin of SingleRandomAccess_LCG section.
Node(s) with error 0
Node selected 3
Single GUP/s 0.103273
Current time (1527648013) is Tue May 29 23:40:13 2018

End of SingleRandomAccess_LCG section.
Begin of PTRANS section.
M: 23040
N: 23040
MB: 512
NB: 512
P: 1
Q: 4
TIME   M     N    MB  NB  P   Q     TIME   CHECK   GB/s   RESID
---- ----- ----- --- --- --- --- -------- ------ -------- -----
WALL 23040 23040 512 512   1   4     1.46 PASSED    2.915  0.00
CPU  23040 23040 512 512   1   4     1.46 PASSED    2.915  0.00
WALL 23040 23040 512 512   1   4     1.46 PASSED    2.907  0.00
CPU  23040 23040 512 512   1   4     1.46 PASSED    2.907  0.00
WALL 23040 23040 512 512   1   4     1.46 PASSED    2.907  0.00
CPU  23040 23040 512 512   1   4     1.46 PASSED    2.916  0.00
WALL 23040 23040 512 512   1   4     1.44 PASSED    2.907  0.00
CPU  23040 23040 512 512   1   4     1.44 PASSED    2.949  0.00
WALL 23040 23040 512 512   1   4     1.46 PASSED    2.907  0.00
CPU  23040 23040 512 512   1   4     1.46 PASSED    2.916  0.00

Finished    5 tests, with the following results:
    5 tests completed and passed residual checks.
    0 tests completed and failed residual checks.
    0 tests skipped because of illegal input values.

END OF TESTS.
Current time (1527648092) is Tue May 29 23:41:32 2018

End of PTRANS section.
Begin of StarDGEMM section.
Scaled residual: 0.00588108
Node(s) with error 0
Minimum Gflop/s 195.036119
Average Gflop/s 196.013815
Maximum Gflop/s 197.379019
Current time (1527648131) is Tue May 29 23:42:11 2018

End of StarDGEMM section.
Begin of SingleDGEMM section.
Node(s) with error 0
Node selected 2
Single DGEMM Gflop/s 204.074092
Current time (1527648168) is Tue May 29 23:42:48 2018

End of SingleDGEMM section.
Begin of StarSTREAM section.
-------------------------------------------------------------
This system uses 8 bytes per DOUBLE PRECISION word.
-------------------------------------------------------------
Array size = 176947200, Offset = 0
Total memory required = 3.9551 GiB.
Each test is run 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
The SCALAR value used for this run is 0.420000
-------------------------------------------------------------
Number of Threads requested = 8
-------------------------------------------------------------
Your clock granularity/precision appears to be 1 microseconds.
Each test below will take on the order of 68604 microseconds.
   (= 68604 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
VERBOSE: total setup time for rank 0 = 0.401080 seconds
-------------------------------------------------------------
Function      Rate (GB/s)   Avg time     Min time     Max time
Copy:          33.2821       0.0857       0.0851       0.0862
Scale:         28.7009       0.1002       0.0986       0.1016
Add:           32.6909       0.1308       0.1299       0.1335
Triad:         32.7955       0.1310       0.1295       0.1383
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays
-------------------------------------------------------------
Node(s) with error 0
Minimum Copy GB/s 33.282091
Average Copy GB/s 33.282091
Maximum Copy GB/s 33.282091
Minimum Scale GB/s 28.700926
Average Scale GB/s 28.700926
Maximum Scale GB/s 28.700926
Minimum Add GB/s 32.690873
Average Add GB/s 32.690873
Maximum Add GB/s 32.690873
Minimum Triad GB/s 32.795500
Average Triad GB/s 32.795500
Maximum Triad GB/s 32.795500
Current time (1527648174) is Tue May 29 23:42:54 2018

End of StarSTREAM section.
Begin of SingleSTREAM section.
Node(s) with error 0
Node selected 3
Single STREAM Copy GB/s 38.466453
Single STREAM Scale GB/s 30.216688
Single STREAM Add GB/s 34.017109
Single STREAM Triad GB/s 34.068779
Current time (1527648178) is Tue May 29 23:42:58 2018

End of SingleSTREAM section.
Begin of MPIFFT section.
Number of nodes: 4
Vector size:            134217728
Generation time:     0.844
Tuning:     0.220
Computing:     1.467
Inverse FFT:     1.564
max(|x-x0|): 2.399e-15
Gflop/s:    12.354
Current time (1527648184) is Tue May 29 23:43:04 2018

End of MPIFFT section.
Begin of StarFFT section.
Vector size: 67108864
Generation time:     1.692
Tuning:     0.001
Computing:     0.651
Inverse FFT:     0.869
max(|x-x0|): 2.360e-15
Node(s) with error 0
Minimum Gflop/s 12.979162
Average Gflop/s 13.264477
Maximum Gflop/s 13.396164
Current time (1527648189) is Tue May 29 23:43:09 2018

End of StarFFT section.
Begin of SingleFFT section.
Node(s) with error 0
Node selected 1
Single FFT Gflop/s 13.349926
Current time (1527648194) is Tue May 29 23:43:14 2018

End of SingleFFT section.
Begin of LatencyBandwidth section.

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Details - level 2
-----------------

MPI_Wtime granularity.
Max. MPI_Wtick is 0.000000 sec
wtick is set to   0.000001 sec  

Message Length: 8
Latency   min / avg / max:   0.001135 /   0.001135 /   0.001135 msecs
Bandwidth min / avg / max:      7.049 /      7.049 /      7.049 MByte/s

MPI_Wtime granularity is ok.
message size:                                  8
max time :                             10.000000 secs
latency for msg:                        0.001135 msecs
estimation for ping pong:               0.102144 msecs
max number of ping pong pairs       =      97901
max client pings = max server pongs =        312
stride for latency                  =          1
Message Length: 8
Latency   min / avg / max:   0.001026 /   0.001112 /   0.001209 msecs
Bandwidth min / avg / max:      6.617 /      7.213 /      7.799 MByte/s

Message Length: 2000000
Latency   min / avg / max:   0.417825 /   0.417825 /   0.417825 msecs
Bandwidth min / avg / max:   4786.690 /   4786.690 /   4786.690 MByte/s

MPI_Wtime granularity is ok.
message size:                            2000000
max time :                             30.000000 secs
latency for msg:                        0.417825 msecs
estimation for ping pong:               3.342602 msecs
max number of ping pong pairs       =       8975
max client pings = max server pongs =         94
stride for latency                  =          1
Message Length: 2000000
Latency   min / avg / max:   0.388213 /   0.427525 /   0.466505 msecs
Bandwidth min / avg / max:   4287.203 /   4694.708 /   5151.811 MByte/s

Message Size:                           8 Byte
Natural Order Latency:           0.001690 msec
Natural Order Bandwidth:         4.733268 MB/s
Avg Random Order Latency:        0.001679 msec
Avg Random Order Bandwidth:      4.765917 MB/s

Message Size:                     2000000 Byte
Natural Order Latency:           0.837921 msec
Natural Order Bandwidth:      2386.860716 MB/s
Avg Random Order Latency:        0.915462 msec
Avg Random Order Bandwidth:   2184.689702 MB/s

Execution time (wall clock)      =     0.431 sec on 4 processes
 - for cross ping_pong latency   =     0.002 sec
 - for cross ping_pong bandwidth =     0.046 sec
 - for ring latency              =     0.014 sec
 - for ring bandwidth            =     0.369 sec

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Major Benchmark results:
------------------------

Max Ping Pong Latency:                 0.001209 msecs
Randomly Ordered Ring Latency:         0.001679 msecs
Min Ping Pong Bandwidth:            4287.203171 MB/s
Naturally Ordered Ring Bandwidth:   2386.860716 MB/s
Randomly  Ordered Ring Bandwidth:   2184.689702 MB/s

------------------------------------------------------------------

Detailed benchmark results:
Ping Pong:
Latency   min / avg / max:   0.001026 /   0.001112 /   0.001209 msecs
Bandwidth min / avg / max:   4287.203 /   4694.708 /   5151.811 MByte/s
Ring:
On naturally ordered ring: latency=      0.001690 msec, bandwidth=   2386.860716 MB/s
On randomly  ordered ring: latency=      0.001679 msec, bandwidth=   2184.689702 MB/s

------------------------------------------------------------------

Benchmark conditions:
 The latency   measurements were done with        8 bytes
 The bandwidth measurements were done with  2000000 bytes
 The ring communication was done in both directions on 4 processes
 The Ping Pong measurements were done on 
  -          12 pairs of processes for latency benchmarking, and 
  -          12 pairs of processes for bandwidth benchmarking, 
 out of 4*(4-1) =         12 possible combinations on 4 processes.
 (1 MB/s = 10**6 byte/sec)

------------------------------------------------------------------
Current time (1527648195) is Tue May 29 23:43:15 2018

End of LatencyBandwidth section.
Begin of HPL section.
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       1 
Q      :       4 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11R6C8       46080   512     1     4             101.08              6.453e+02
--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0032497 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
Current time (1527648316) is Tue May 29 23:45:16 2018

End of HPL section.
Begin of Summary section.
VersionMajor=1
VersionMinor=5
VersionMicro=0
VersionRelease=f
LANG=C
Success=1
sizeof_char=1
sizeof_short=2
sizeof_int=4
sizeof_long=8
sizeof_void_ptr=8
sizeof_size_t=8
sizeof_float=4
sizeof_double=8
sizeof_s64Int=8
sizeof_u64Int=8
sizeof_struct_double_double=16
CommWorldProcs=4
MPI_Wtick=1.000000e-09
HPL_Tflops=0.645331
HPL_time=101.085
HPL_eps=1.11022e-16
HPL_RnormI=6.31859e-10
HPL_Anorm1=11637.5
HPL_AnormI=11641.7
HPL_Xnorm1=29866.1
HPL_XnormI=3.26464
HPL_BnormI=0.499991
HPL_N=46080
HPL_NB=512
HPL_nprow=1
HPL_npcol=4
HPL_depth=1
HPL_nbdiv=6
HPL_nbmin=8
HPL_cpfact=C
HPL_crfact=R
HPL_ctop=1
HPL_order=R
HPL_dMACH_EPS=1.110223e-16
HPL_dMACH_SFMIN=2.225074e-308
HPL_dMACH_BASE=2.000000e+00
HPL_dMACH_PREC=2.220446e-16
HPL_dMACH_MLEN=5.300000e+01
HPL_dMACH_RND=1.000000e+00
HPL_dMACH_EMIN=-1.021000e+03
HPL_dMACH_RMIN=2.225074e-308
HPL_dMACH_EMAX=1.024000e+03
HPL_dMACH_RMAX=1.797693e+308
HPL_sMACH_EPS=5.960464e-08
HPL_sMACH_SFMIN=1.175494e-38
HPL_sMACH_BASE=2.000000e+00
HPL_sMACH_PREC=1.192093e-07
HPL_sMACH_MLEN=2.400000e+01
HPL_sMACH_RND=1.000000e+00
HPL_sMACH_EMIN=-1.250000e+02
HPL_sMACH_RMIN=1.175494e-38
HPL_sMACH_EMAX=1.280000e+02
HPL_sMACH_RMAX=3.402823e+38
dweps=1.110223e-16
sweps=5.960464e-08
HPLMaxProcs=4
HPLMinProcs=4
DGEMM_N=13301
StarDGEMM_Gflops=196.014
SingleDGEMM_Gflops=204.074
PTRANS_GBs=2.90706
PTRANS_time=1.45627
PTRANS_residual=0
PTRANS_n=23040
PTRANS_nb=512
PTRANS_nprow=1
PTRANS_npcol=4
MPIRandomAccess_LCG_N=1073741824
MPIRandomAccess_LCG_time=58.9437
MPIRandomAccess_LCG_CheckTime=10.4178
MPIRandomAccess_LCG_Errors=0
MPIRandomAccess_LCG_ErrorsFraction=0
MPIRandomAccess_LCG_ExeUpdates=1649620204
MPIRandomAccess_LCG_GUPs=0.0279864
MPIRandomAccess_LCG_TimeBound=60
MPIRandomAccess_LCG_Algorithm=0
MPIRandomAccess_N=1073741824
MPIRandomAccess_time=58.7687
MPIRandomAccess_CheckTime=10.8231
MPIRandomAccess_Errors=0
MPIRandomAccess_ErrorsFraction=0
MPIRandomAccess_ExeUpdates=1626629596
MPIRandomAccess_GUPs=0.0276785
MPIRandomAccess_TimeBound=60
MPIRandomAccess_Algorithm=0
RandomAccess_LCG_N=268435456
StarRandomAccess_LCG_GUPs=0.0948124
SingleRandomAccess_LCG_GUPs=0.103273
RandomAccess_N=268435456
StarRandomAccess_GUPs=0.0941938
SingleRandomAccess_GUPs=0.0945806
STREAM_VectorSize=176947200
STREAM_Threads=8
StarSTREAM_Copy=33.2821
StarSTREAM_Scale=28.7009
StarSTREAM_Add=32.6909
StarSTREAM_Triad=32.7955
SingleSTREAM_Copy=38.4665
SingleSTREAM_Scale=30.2167
SingleSTREAM_Add=34.0171
SingleSTREAM_Triad=34.0688
FFT_N=67108864
StarFFT_Gflops=13.2645
SingleFFT_Gflops=13.3499
MPIFFT_N=134217728
MPIFFT_Gflops=12.3539
MPIFFT_maxErr=2.39909e-15
MPIFFT_Procs=4
MaxPingPongLatency_usec=1.20909
RandomlyOrderedRingLatency_usec=1.67859
MinPingPongBandwidth_GBytes=4.2872
NaturallyOrderedRingBandwidth_GBytes=2.38686
RandomlyOrderedRingBandwidth_GBytes=2.18469
MinPingPongLatency_usec=1.02579
AvgPingPongLatency_usec=1.11237
MaxPingPongBandwidth_GBytes=5.15181
AvgPingPongBandwidth_GBytes=4.69471
NaturallyOrderedRingLatency_usec=1.69016
FFTEnblk=16
FFTEnp=8
FFTEl2size=1048576
M_OPENMP=201511
omp_get_num_threads=8
omp_get_max_threads=8
omp_get_num_procs=8
MemProc=-1
MemSpec=-1
MemVal=-1
MPIFFT_time0=3.00258e-06
MPIFFT_time1=0.343253
MPIFFT_time2=0.169804
MPIFFT_time3=0.23949
MPIFFT_time4=0.35749
MPIFFT_time5=0.276735
MPIFFT_time6=6.97561e-07
CPS_HPCC_FFT_235=0
CPS_HPCC_FFTW_ESTIMATE=0
CPS_HPCC_MEMALLCTR=0
CPS_HPL_USE_GETPROCESSTIMES=0
CPS_RA_SANDIA_NOPT=0
CPS_RA_SANDIA_OPT2=0
CPS_USING_FFTW=0
End of Summary section.
########################################################################
End of HPC Challenge tests.
Current time (1527648316) is Tue May 29 23:45:16 2018

########################################################################
