########################################################################
This is the DARPA/DOE HPC Challenge Benchmark version 1.5.0 October 2012
Produced by Jack Dongarra and Piotr Luszczek
Innovative Computing Laboratory
University of Tennessee Knoxville and Oak Ridge National Laboratory

See the source files for authors of specific codes.
Compiled on May 30 2018 at 13:47:29
Current time (1527699348) is Wed May 30 13:55:48 2018

Hostname: 'hpc1-ib'
########################################################################
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       4 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

Begin of MPIRandomAccess section.
Running on 32 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^25 = 33554432 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 4294967296 (for a TIME BOUND of 60.00 secs)
CPU time used = 24.583831 seconds
Real time used = 24.642079 seconds
0.174294031 Billion(10^9) Updates    per second [GUP/s]
0.005446688 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 7.462783 seconds
Verification:  Real time used = 7.472968 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527699381) is Wed May 30 13:56:21 2018

End of MPIRandomAccess section.
Begin of StarRandomAccess section.
Main table size   = 2^25 = 33554432 words
Number of updates = 134217728
CPU time used  = 4.134728 seconds
Real time used = 4.138951 seconds
0.032427954 Billion(10^9) Updates    per second [GUP/s]
Found 0 errors in 33554432 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.032428
Average GUP/s 0.033950
Maximum GUP/s 0.035887
Current time (1527699389) is Wed May 30 13:56:29 2018

End of StarRandomAccess section.
Begin of SingleRandomAccess section.
Node(s) with error 0
Node selected 27
Single GUP/s 0.084635
Current time (1527699392) is Wed May 30 13:56:32 2018

End of SingleRandomAccess section.
Begin of MPIRandomAccess_LCG section.
Running on 32 processors (PowerofTwo)
Total Main table size = 2^30 = 1073741824 words
PE Main table size = 2^25 = 33554432 words/PE
Default number of updates (RECOMMENDED) = 4294967296
Number of updates EXECUTED = 4294967296 (for a TIME BOUND of 60.00 secs)
CPU time used = 24.807953 seconds
Real time used = 24.842458 seconds
0.172888180 Billion(10^9) Updates    per second [GUP/s]
0.005402756 Billion(10^9) Updates/PE per second [GUP/s]
Verification:  CPU time used = 7.329384 seconds
Verification:  Real time used = 7.341820 seconds
Found 0 errors in 1073741824 locations (passed).
Current time (1527699425) is Wed May 30 13:57:05 2018

End of MPIRandomAccess_LCG section.
Begin of StarRandomAccess_LCG section.
Main table size   = 2^25 = 33554432 words
Number of updates = 134217728
CPU time used  = 4.101635 seconds
Real time used = 4.106873 seconds
0.032681248 Billion(10^9) Updates    per second [GUP/s]
Found 0 errors in 33554432 locations (passed).
Node(s) with error 0
Minimum GUP/s 0.032236
Average GUP/s 0.033714
Maximum GUP/s 0.035658
Current time (1527699433) is Wed May 30 13:57:13 2018

End of StarRandomAccess_LCG section.
Begin of SingleRandomAccess_LCG section.
Node(s) with error 0
Node selected 4
Single GUP/s 0.081748
Current time (1527699436) is Wed May 30 13:57:16 2018

End of SingleRandomAccess_LCG section.
Begin of PTRANS section.
M: 23040
N: 23040
MB: 512
NB: 512
P: 4
Q: 8
TIME   M     N    MB  NB  P   Q     TIME   CHECK   GB/s   RESID
---- ----- ----- --- --- --- --- -------- ------ -------- -----
WALL 23040 23040 512 512   4   8     0.66 PASSED    6.478  0.00
CPU  23040 23040 512 512   4   8     0.65 PASSED    6.493  0.00
WALL 23040 23040 512 512   4   8     0.58 PASSED    6.478  0.00
CPU  23040 23040 512 512   4   8     0.58 PASSED    7.290  0.00
WALL 23040 23040 512 512   4   8     0.53 PASSED    6.478  0.00
CPU  23040 23040 512 512   4   8     0.53 PASSED    8.071  0.00
WALL 23040 23040 512 512   4   8     0.62 PASSED    6.478  0.00
CPU  23040 23040 512 512   4   8     0.62 PASSED    6.864  0.00
WALL 23040 23040 512 512   4   8     0.53 PASSED    6.478  0.00
CPU  23040 23040 512 512   4   8     0.53 PASSED    8.052  0.00

Finished    5 tests, with the following results:
    5 tests completed and passed residual checks.
    0 tests completed and failed residual checks.
    0 tests skipped because of illegal input values.

END OF TESTS.
Current time (1527699452) is Wed May 30 13:57:32 2018

End of PTRANS section.
Begin of StarDGEMM section.
Scaled residual: 0.0094354
Node(s) with error 0
Minimum Gflop/s 22.883225
Average Gflop/s 22.971181
Maximum Gflop/s 23.032343
Current time (1527699463) is Wed May 30 13:57:43 2018

End of StarDGEMM section.
Begin of SingleDGEMM section.
Node(s) with error 0
Node selected 12
Single DGEMM Gflop/s 26.259360
Current time (1527699473) is Wed May 30 13:57:53 2018

End of SingleDGEMM section.
Begin of StarSTREAM section.
-------------------------------------------------------------
This system uses 8 bytes per DOUBLE PRECISION word.
-------------------------------------------------------------
Array size = 22118400, Offset = 0
Total memory required = 0.4944 GiB.
Each test is run 10 times.
 The *best* time for each kernel (excluding the first iteration)
 will be used to compute the reported bandwidth.
The SCALAR value used for this run is 0.420000
-------------------------------------------------------------
Number of Threads requested = 1
-------------------------------------------------------------
Your clock granularity/precision appears to be 1 microseconds.
Each test below will take on the order of 67472 microseconds.
   (= 67472 clock ticks)
Increase the size of the arrays if this shows that
you are not getting at least 20 clock ticks per test.
-------------------------------------------------------------
WARNING -- The above is only a rough guideline.
For best results, please be sure you know the
precision of your system timer.
-------------------------------------------------------------
VERBOSE: total setup time for rank 0 = 0.403097 seconds
-------------------------------------------------------------
Function      Rate (GB/s)   Avg time     Min time     Max time
Copy:           4.2010       0.0871       0.0842       0.0994
Scale:          3.6625       0.0972       0.0966       0.0979
Add:            4.2869       0.1252       0.1238       0.1292
Triad:          4.2890       0.1242       0.1238       0.1247
-------------------------------------------------------------
Solution Validates: avg error less than 1.000000e-13 on all three arrays
-------------------------------------------------------------
Node(s) with error 0
Minimum Copy GB/s 4.200975
Average Copy GB/s 4.200975
Maximum Copy GB/s 4.200975
Minimum Scale GB/s 3.662476
Average Scale GB/s 3.662476
Maximum Scale GB/s 3.662476
Minimum Add GB/s 4.286935
Average Add GB/s 4.286935
Maximum Add GB/s 4.286935
Minimum Triad GB/s 4.288955
Average Triad GB/s 4.288955
Maximum Triad GB/s 4.288955
Current time (1527699478) is Wed May 30 13:57:58 2018

End of StarSTREAM section.
Begin of SingleSTREAM section.
Node(s) with error 0
Node selected 24
Single STREAM Copy GB/s 5.827931
Single STREAM Scale GB/s 13.196734
Single STREAM Add GB/s 13.468702
Single STREAM Triad GB/s 13.403599
Current time (1527699480) is Wed May 30 13:58:00 2018

End of SingleSTREAM section.
Begin of MPIFFT section.
Number of nodes: 32
Vector size:            134217728
Generation time:     0.123
Tuning:     0.053
Computing:     1.290
Inverse FFT:     1.327
max(|x-x0|): 4.707e-15
Gflop/s:    14.043
Current time (1527699483) is Wed May 30 13:58:03 2018

End of MPIFFT section.
Begin of StarFFT section.
Vector size: 8388608
Generation time:     0.245
Tuning:     0.000
Computing:     0.487
Inverse FFT:     0.571
max(|x-x0|): 4.120e-15
Node(s) with error 0
Minimum Gflop/s 1.818637
Average Gflop/s 1.971317
Maximum Gflop/s 2.089049
Current time (1527699485) is Wed May 30 13:58:05 2018

End of StarFFT section.
Begin of SingleFFT section.
Node(s) with error 0
Node selected 8
Single FFT Gflop/s 2.675062
Current time (1527699486) is Wed May 30 13:58:06 2018

End of SingleFFT section.
Begin of LatencyBandwidth section.

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Details - level 2
-----------------

MPI_Wtime granularity.
Max. MPI_Wtick is 0.000000 sec
wtick is set to   0.000001 sec  

Message Length: 8
Latency   min / avg / max:   0.001052 /   0.001052 /   0.001052 msecs
Bandwidth min / avg / max:      7.605 /      7.605 /      7.605 MByte/s

MPI_Wtime granularity is ok.
message size:                                  8
max time :                             10.000000 secs
latency for msg:                        0.001052 msecs
estimation for ping pong:               0.094674 msecs
max number of ping pong pairs       =     105626
max client pings = max server pongs =        325
stride for latency                  =          1
Message Length: 8
Latency   min / avg / max:   0.000369 /   0.000922 /   0.001271 msecs
Bandwidth min / avg / max:      6.296 /     10.205 /     21.705 MByte/s

Message Length: 2000000
Latency   min / avg / max:   0.417372 /   0.417372 /   0.417372 msecs
Bandwidth min / avg / max:   4791.891 /   4791.891 /   4791.891 MByte/s

MPI_Wtime granularity is ok.
message size:                            2000000
max time :                             30.000000 secs
latency for msg:                        0.417372 msecs
estimation for ping pong:               3.338974 msecs
max number of ping pong pairs       =       8984
max client pings = max server pongs =         94
stride for latency                  =          1
Message Length: 2000000
Latency   min / avg / max:   0.188257 /   0.365683 /   0.460569 msecs
Bandwidth min / avg / max:   4342.456 /   6088.327 /  10623.770 MByte/s

Message Size:                           8 Byte
Natural Order Latency:           0.002110 msec
Natural Order Bandwidth:         3.791150 MB/s
Avg Random Order Latency:        0.002730 msec
Avg Random Order Bandwidth:      2.929995 MB/s

Message Size:                     2000000 Byte
Natural Order Latency:           1.376764 msec
Natural Order Bandwidth:      1452.682129 MB/s
Avg Random Order Latency:        4.862797 msec
Avg Random Order Bandwidth:    411.285959 MB/s

Execution time (wall clock)      =     5.330 sec on 32 processes
 - for cross ping_pong latency   =     0.203 sec
 - for cross ping_pong bandwidth =     3.020 sec
 - for ring latency              =     0.032 sec
 - for ring bandwidth            =     2.075 sec

------------------------------------------------------------------
Latency-Bandwidth-Benchmark R1.5.1 (c) HLRS, University of Stuttgart
Written by Rolf Rabenseifner, Gerrit Schulz, and Michael Speck, Germany

Major Benchmark results:
------------------------

Max Ping Pong Latency:                 0.001271 msecs
Randomly Ordered Ring Latency:         0.002730 msecs
Min Ping Pong Bandwidth:            4342.456399 MB/s
Naturally Ordered Ring Bandwidth:   1452.682129 MB/s
Randomly  Ordered Ring Bandwidth:    411.285959 MB/s

------------------------------------------------------------------

Detailed benchmark results:
Ping Pong:
Latency   min / avg / max:   0.000369 /   0.000922 /   0.001271 msecs
Bandwidth min / avg / max:   4342.456 /   6088.327 /  10623.770 MByte/s
Ring:
On naturally ordered ring: latency=      0.002110 msec, bandwidth=   1452.682129 MB/s
On randomly  ordered ring: latency=      0.002730 msec, bandwidth=    411.285959 MB/s

------------------------------------------------------------------

Benchmark conditions:
 The latency   measurements were done with        8 bytes
 The bandwidth measurements were done with  2000000 bytes
 The ring communication was done in both directions on 32 processes
 The Ping Pong measurements were done on 
  -         992 pairs of processes for latency benchmarking, and 
  -         992 pairs of processes for bandwidth benchmarking, 
 out of 32*(32-1) =        992 possible combinations on 32 processes.
 (1 MB/s = 10**6 byte/sec)

------------------------------------------------------------------
Current time (1527699491) is Wed May 30 13:58:11 2018

End of LatencyBandwidth section.
Begin of HPL section.
================================================================================
HPLinpack 2.0  --  High-Performance Linpack benchmark  --   September 10, 2008
Written by A. Petitet and R. Clint Whaley,  Innovative Computing Laboratory, UTK
Modified by Piotr Luszczek, Innovative Computing Laboratory, UTK
Modified by Julien Langou, University of Colorado Denver
================================================================================

An explanation of the input/output parameters follows:
T/V    : Wall time / encoded variant.
N      : The order of the coefficient matrix A.
NB     : The partitioning blocking factor.
P      : The number of process rows.
Q      : The number of process columns.
Time   : Time in seconds to solve the linear system.
Gflops : Rate of execution for solving the linear system.

The following parameter values will be used:

N      :   46080 
NB     :     512 
PMAP   : Row-major process mapping
P      :       4 
Q      :       8 
PFACT  :   Crout 
NBMIN  :       8 
NDIV   :       6 
RFACT  :   Right 
BCAST  :  1ringM 
DEPTH  :       1 
SWAP   : Mix (threshold = 16)
L1     : no-transposed form
U      : transposed form
EQUIL  : yes
ALIGN  : 8 double precision words

--------------------------------------------------------------------------------

- The matrix A is randomly generated for each test.
- The following scaled residual check will be computed:
      ||Ax-b||_oo / ( eps * ( || x ||_oo * || A ||_oo + || b ||_oo ) * N )
- The relative machine precision (eps) is taken to be               1.110223e-16
- Computational tests pass if scaled residuals are less than                16.0

================================================================================
T/V                N    NB     P     Q               Time                 Gflops
--------------------------------------------------------------------------------
WR11R6C8       46080   512     4     8             105.81              6.165e+02
--------------------------------------------------------------------------------
||Ax-b||_oo/(eps*(||A||_oo*||x||_oo+||b||_oo)*N)=        0.0030950 ...... PASSED
================================================================================

Finished      1 tests with the following results:
              1 tests completed and passed residual checks,
              0 tests completed and failed residual checks,
              0 tests skipped because of illegal input values.
--------------------------------------------------------------------------------

End of Tests.
================================================================================
Current time (1527699600) is Wed May 30 14:00:00 2018

End of HPL section.
Begin of Summary section.
VersionMajor=1
VersionMinor=5
VersionMicro=0
VersionRelease=f
LANG=C
Success=1
sizeof_char=1
sizeof_short=2
sizeof_int=4
sizeof_long=8
sizeof_void_ptr=8
sizeof_size_t=8
sizeof_float=4
sizeof_double=8
sizeof_s64Int=8
sizeof_u64Int=8
sizeof_struct_double_double=16
CommWorldProcs=32
MPI_Wtick=1.000000e-09
HPL_Tflops=0.616487
HPL_time=105.814
HPL_eps=1.11022e-16
HPL_RnormI=6.0179e-10
HPL_Anorm1=11637.5
HPL_AnormI=11641.7
HPL_Xnorm1=29866.1
HPL_XnormI=3.26464
HPL_BnormI=0.499991
HPL_N=46080
HPL_NB=512
HPL_nprow=4
HPL_npcol=8
HPL_depth=1
HPL_nbdiv=6
HPL_nbmin=8
HPL_cpfact=C
HPL_crfact=R
HPL_ctop=1
HPL_order=R
HPL_dMACH_EPS=1.110223e-16
HPL_dMACH_SFMIN=2.225074e-308
HPL_dMACH_BASE=2.000000e+00
HPL_dMACH_PREC=2.220446e-16
HPL_dMACH_MLEN=5.300000e+01
HPL_dMACH_RND=1.000000e+00
HPL_dMACH_EMIN=-1.021000e+03
HPL_dMACH_RMIN=2.225074e-308
HPL_dMACH_EMAX=1.024000e+03
HPL_dMACH_RMAX=1.797693e+308
HPL_sMACH_EPS=5.960464e-08
HPL_sMACH_SFMIN=1.175494e-38
HPL_sMACH_BASE=2.000000e+00
HPL_sMACH_PREC=1.192093e-07
HPL_sMACH_MLEN=2.400000e+01
HPL_sMACH_RND=1.000000e+00
HPL_sMACH_EMIN=-1.250000e+02
HPL_sMACH_RMIN=1.175494e-38
HPL_sMACH_EMAX=1.280000e+02
HPL_sMACH_RMAX=3.402823e+38
dweps=1.110223e-16
sweps=5.960464e-08
HPLMaxProcs=32
HPLMinProcs=32
DGEMM_N=4702
StarDGEMM_Gflops=22.9712
SingleDGEMM_Gflops=26.2594
PTRANS_GBs=6.47843
PTRANS_time=0.529537
PTRANS_residual=0
PTRANS_n=23040
PTRANS_nb=512
PTRANS_nprow=4
PTRANS_npcol=8
MPIRandomAccess_LCG_N=1073741824
MPIRandomAccess_LCG_time=24.8425
MPIRandomAccess_LCG_CheckTime=7.34182
MPIRandomAccess_LCG_Errors=0
MPIRandomAccess_LCG_ErrorsFraction=0
MPIRandomAccess_LCG_ExeUpdates=4294967296
MPIRandomAccess_LCG_GUPs=0.172888
MPIRandomAccess_LCG_TimeBound=60
MPIRandomAccess_LCG_Algorithm=0
MPIRandomAccess_N=1073741824
MPIRandomAccess_time=24.6421
MPIRandomAccess_CheckTime=7.47297
MPIRandomAccess_Errors=0
MPIRandomAccess_ErrorsFraction=0
MPIRandomAccess_ExeUpdates=4294967296
MPIRandomAccess_GUPs=0.174294
MPIRandomAccess_TimeBound=60
MPIRandomAccess_Algorithm=0
RandomAccess_LCG_N=33554432
StarRandomAccess_LCG_GUPs=0.0337141
SingleRandomAccess_LCG_GUPs=0.0817481
RandomAccess_N=33554432
StarRandomAccess_GUPs=0.0339502
SingleRandomAccess_GUPs=0.0846349
STREAM_VectorSize=22118400
STREAM_Threads=1
StarSTREAM_Copy=4.20097
StarSTREAM_Scale=3.66248
StarSTREAM_Add=4.28694
StarSTREAM_Triad=4.28896
SingleSTREAM_Copy=5.82793
SingleSTREAM_Scale=13.1967
SingleSTREAM_Add=13.4687
SingleSTREAM_Triad=13.4036
FFT_N=8388608
StarFFT_Gflops=1.97132
SingleFFT_Gflops=2.67506
MPIFFT_N=134217728
MPIFFT_Gflops=14.0426
MPIFFT_maxErr=4.70749e-15
MPIFFT_Procs=32
MaxPingPongLatency_usec=1.27074
RandomlyOrderedRingLatency_usec=2.73038
MinPingPongBandwidth_GBytes=4.34246
NaturallyOrderedRingBandwidth_GBytes=1.45268
RandomlyOrderedRingBandwidth_GBytes=0.411286
MinPingPongLatency_usec=0.368571
AvgPingPongLatency_usec=0.922445
MaxPingPongBandwidth_GBytes=10.6238
AvgPingPongBandwidth_GBytes=6.08833
NaturallyOrderedRingLatency_usec=2.11018
FFTEnblk=16
FFTEnp=8
FFTEl2size=1048576
M_OPENMP=201611
omp_get_num_threads=1
omp_get_max_threads=1
omp_get_num_procs=1
MemProc=-1
MemSpec=-1
MemVal=-1
MPIFFT_time0=2.0247e-06
MPIFFT_time1=0.260911
MPIFFT_time2=0.121626
MPIFFT_time3=0.225714
MPIFFT_time4=0.345833
MPIFFT_time5=0.299925
MPIFFT_time6=1.32713e-06
CPS_HPCC_FFT_235=0
CPS_HPCC_FFTW_ESTIMATE=0
CPS_HPCC_MEMALLCTR=0
CPS_HPL_USE_GETPROCESSTIMES=0
CPS_RA_SANDIA_NOPT=0
CPS_RA_SANDIA_OPT2=0
CPS_USING_FFTW=0
End of Summary section.
########################################################################
End of HPC Challenge tests.
Current time (1527699600) is Wed May 30 14:00:00 2018

########################################################################
