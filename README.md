# WHAT IS THIS?

A bunch of scripts that knows how to deal with the HPCC Benchmark: running test batteries, saving and plotting the output data from the tests. That's about it.

You have three main folders to worry about:

- `hpcc/`: Contains the HPCC Benchmark v1.5.0 itself, nothing more, nothing less.
- `output/`: Folder where the output data is saved by default, if you run the tests using the script `scripts/hpcc.sh`. We've left a bunch of our own tests so you can try to use the plotting scripts from `scripts/` while your own tests are running.
- `scripts/`: Contains the scripts that 1) runs and save, 2) analyzes and 3) plots the output data. [More detail below](#what-each-script-does).
- `scripts/dummies/`: Contains two dummy files (templates) for running the tests (dummyinf.txt) and plotting the data (gnuplot.script.dummy).
- `scripts/hpl-makefiles/`: Contains four Makefile templates that worked in the HPC1 and HPC2 cluster. They'll surely need to be adapted.

## WHAT YOU NEED TO KNOW ABOUT HPCC

The benchmark is in the folder `hpcc/`, and to compile it, you need to create and configure a custom Makefile in `hpcc/hpl/`, with the name `Make.<whatever_you_want>`. The folder `hpcc/hpl/setup/` has some templates, but I've prepared four and left them in two places: 1) Inside `hpcc/hpl/`, and 2) inside `scripts/hpl-makefiles`. Their name tells what configuration they have, basically.

To compile the benchmark, it's the same as when compiling HPL:

```bash
$ make arch=<ARCH>
```

Where `<ARCH>` will be the `<whatever_you_want>` part said above. If you want to recompile, just run

```bash
$ make arch=<ARCH> clean all
```

If no errors occur, the executable `hpcc` should appear inside the `hpcc/` folder (yes, it'll be `hpcc/hpcc`). Now, all you need to do is to configure the `hpccinf.txt` file and run the executable using a MPI library or a job scheduler (srun, mpirun, mpiexec.hydra, ...).

While a test is running, the HPCC will constantly print data in the `hpccoutf.txt` file. Use `tail` to keep track of which benchmark is running. You'll need the `scripts/compiler.pl` official script to understand the gibberish written there.

Note that if you run two tests in a row without moving/erasing `hpccoutf.txt`, the results for the new run will be **appended** to the file, leaving it's original contents intact, and thus only increasing the file size without need.

## WHAT EACH SCRIPT DOES

- `hpcc.sh`: Runs and save test results. You need to configure a series of things, which are all marked inside the script itself, inside a "comment block". Here's an example:

```
####################################################
# BEGIN BLOCK (WHAT YOU NEED TO CONFIGURE)
####################################################
# 
# Everything inside this block might need custom
# configuration.
#
# I've left information about what you need to do
# in each block, which might help you or not.
#
####################################################
# END BLOCK (WHAT YOU NEED TO CONFIGURE)
####################################################
```

In addition, the `hpcc.sh` by default runs each test 3 times, and saves the output files with the suffix `_run-1.txt`. `_run-2.txt` and `_run-3.txt`. This pattern is used by the script `prepare_data.sh`, so be careful if you're planning to change that!

- `prepare_data.sh`: Almost self explanatory, but not quite. This script only gathers a bunch of runs of the same test (using the pattern above) and saves the average value. By default, it takes the data from the folder `output/`. Run `./prepare_data.sh --help` for info about how to customize what to analyze and where to save it.

- `plot.sh`: Gathers the data prepared by `prepare_data.sh` and plots it using `gnuplot`. By default, it plots SVGs and saves them in the folder `output/img/`. There are many options to change: run `./plot.sh --help` to get a list of options.

- `compiler.pl`: It's the HPCC official output compiler. It resumes the data from the `hpccoutf.txt` and transforms it in a nice table:

```
--------------------------------------------------------------------------------------------------
HPCCOUTF NAME                        WEB NAME                      VALUE   UNITS
--------------------------------------------------------------------------------------------------
HPL_Tflops                           G-HPL                        1.2113   Tera Flops per Second
PTRANS_GBs                           G-PTRANS                     0.0047   Tera Bytes per Second
MPIRandomAccess_GUPs                 G-RandomAccess               0.0155   Giga Updates per Second
MPIFFT_Gflops                        G-FFT                        0.0105   Tera Flops per Second
StarSTREAM_Triad*CommWorldProcs      EP-STREAM Sys                0.1040   Tera Bytes per Second
StarSTREAM_Triad                     EP-STREAM Triad             13.0008   Giga Bytes per Second
StarDGEMM_Gflops                     EP-DGEMM                   189.1360   Giga Flops per Second
RandomlyOrderedRingBandwidth_GBytes  RandomRing Bandwidth         2.3167   Giga Bytes per second
RandomlyOrderedRingLatency_usec      RandomRing Latency           1.0655   micro-seconds
--------------------------------------------------------------------------------------------------
```

It's used automagically by `hpcc.sh`, but if you want to use it yourself, the usage is `perl compiler.pl -w -f hpccoutf.txt` for the nice table, or `perl compiler.pl -a -f hpccoutf.txt` for the complete list of all the results for each benchmark.

- `n_calculator.py`: A simple calculator for the parameter N (problem size) of the HPCC configuration file, `hpccinf.txt`.

## WHAT SHOULD I TEST?

If you look at the file `hpcc/hpccinf.txt`, you'll notice the gigantic amount of parameters to vary.

We tested a bunch of configurations and asked Google and Google Scholar a lot of questions, and came to some conclusions to help diminishing the amount of parameters to test:

1. Parameter `N`: Problem size. Use the `n_calculator.py` to set it. The recommended value is around 90% of **one node's total memory**, i.e. a value that will consume 250GB in one node will consume 500GB in two nodes, being 250GB in each node. If you're going to test parameter variation, it's better to use values around 16GB, as this parameter only affects the execution time and memory consumption (the impact on the results for 16GB and above are pratically null)
2. Parameter `NB`: Block size, e.g. "size of the matrix block to be given for each process to solve per time". This value affects which N should be used, because "N must be a multiple of NB" in order to have a perfect distribution. Recommended values are: 1) 512, from our tests, and 2) 224, from some [not-so-trustworthy websites](http://hpl-calculator.sourceforge.net/). Again, the `n_calculator.py` does these N and NB roundings for you.
3. Parameter `PMAP`: In which direction the processes should process the matrix: 0 for Row-major (from left-right, top-down order [Z]) or 1 for Column-major (from top-down, left-right order [И]). For Intel systems, row-major is recommended.
4. Process Grid `PxQ`: PxQ == number of processes. If you have a parallel interconnection (i.e. a switch or InfiniBand connecting your nodes), Q must be equal or slightly bigger than P (e.g. for 64 processes, 8x8 or 4x16). If it's a single bus, flatter values are recommended (e.g. 2x32, 1x64). In general, a square PxQ had better results.
5. Parameter `BCAST`: 1 (1rM -- Increasing-Ring Modified) or 3 (2rM -- Increasing-Two-Ring Modified). More detail [here](http://www.netlib.org/benchmark/hpl/algorithm.html).
6. Parameter `SWAP`: Recommended is 1 (long). The option 2 will do exactly the same as 1, so any of them will work.
7. Parameter `DEPTH`: 1 is usually the best. 2 might or might not do better. Other values are not recommended.
8. Parameters `L1` and `U`: L1 in no-transposed form (1), U in transposed form (0).
9. Parameters `EQUILIBRATION` and `MEMORY ALIGNMENT`: equilibrated (1) and 8-byte aligned.

The remaining parameters `PFACT`, `NDIV`, `RFACT`, `NBMIN` and `SWAP THRESHOLD` may vary depending on the machine. For the C3HPC cluster, the best values from our tests are set as defaults in the script `hpcc.sh`, but there's absolutely no guarantee that they're the best ones, as our test set was small.

## THINGS YOU SHOULD WORRY ABOUT

### THE OMP_PLACES AND OMP_PROC_BIND ENVIRONMENT VARIABLE

If you plan to test multiple variations of number of process and number of threads, you should take care about the `OMP_PLACES` variable. Depending on it's value, the `mpirun` from OpenMPI will map multiple processes to the same core, and won't use the others.

To avoid that happening, as we don't know if the same will happen to other libraries other than OpenMPI and IntelMPI, the recommendation for the values of `OMP_PLACES` are simple:

Given the number of processes N and number of threads M you want to raise, set N places with M contiguous spaces to `OMP_PLACES`. For example, in a 32 core system, with 4 sockets with 8 cores each, if you want to run a test with 4 processes with 8 threads each, then set:

```bash
export OMP_PLACES="{0:8},{8:8},{16:8},{24:8}"
```

The `{N:M}` setting is a way to define a "place". It can be read as "starting from core N up to M cores ahead", or "Cores [N..(N+M-1)]".

The above setting is telling us that we have only FOUR places to raise processes: Core [0..7], Core [8..15], Core [16..23] and Core [24..31]. So, if the scheduler (say `mpirun`) raise a process on core 5, then for the next one it will avoid cores {0,1,2,3,4,6,7}, as they're part of the same "place". Only when all four places have one process each that it will restart using all of them again.

Just for a second example, if I have the same 32 core system, and want to run a test with 8 processes with 4 threads each, then my OMP_PLACES should be set as follows:

```bash
export OMP_PLACES="{0:4},{4:4},{8:4},{12:4},{16:4},{20:4},{24:4},{28:4}"
```

Note that the places's order doesn't matter, as long as the overall sum is equal to the number of cores and there is no overlap of cores (one core mapped in two places).

Now, for the `OMP_PROC_BIND` variable, the recommended values are either "master" (pin threads as close as possible to the master thread) or "close" (pin related threads as close as possible to each other):

```bash
# Choose between this
export OMP_PROC_BIND=close

# Or this
export OMP_PROC_BIND=master
```

### OTHER ENVIRONMENT VARIABLES

To set the number of threads you want each process to have, you can set the `OMP_NUM_THREADS` variable (for OpenMP) and/or `MKL_NUM_THREADS` variable (for MKL):

```bash
# If your program uses MKL in it's entirety, setting only MKL_NUM_THREADS is enough
export OMP_NUM_THREADS=8
export MKL_NUM_THREADS=8
```

Note that `OMP_NUM_THREADS=8` means that each process will create **7** threads, as the process itself is also a thread (called 'master thread').

If you're using MKL, remember to set both variables `MKL_NUM_THREADS` and `OMP_NUM_THREADS` to the exact same value. The reason is simple: HPCC has a subset of benchmarks that uses MKL and the others doesn't. So, for those who doesn't use it, they'll use the `OMP_NUM_THREADS` variable instead.

To see where each OpenMP thread is being pinned, set the `OMP_DISPLAY_ENV` variable:

```bash
# 'true' for knowing where each thread was pinned
export OMP_DISPLAY_ENV=true

# 'verbose' for knowing every single little movement that happend to the environment
# We don't recommend it as it rewrites a lot of information
export OMP_DISPLAY_ENV=verbose
```

If you're using IntelMPI or MKL, consider using the `KMP_AFFINITY` variable instead of `OMP_PLACES` and `OMP_PROC_BIND`, which has [similar configuration](https://software.intel.com/en-us/articles/using-kmp-affinity-to-create-openmp-thread-mapping-to-os-proc-ids).

## OUR TEST RECOMMENDATIONS

If you're going to use the script `hpcc.sh`, you should place your parameter tests inside the `test_battery()` function, varying a single parameter and selecting the one that gives the best results overall. A sample of what to do in the `test_battery()` function:

```bash
# Tests with one node
NUM_NODES=1

# (4 processes, 8 threads/p)
make_test 2 2 8 "{0:8},{8:8},{16:8},{24:8}"

# (32 processes, 1 thread/p)
make_test 4 8 1 cores


# Tests with two nodes
NUM_NODES=2
    
# (8 processes, 8 threads/p)
PARAMS[PFACT]=0
make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" PFACT-0

PARAMS[PFACT]=1
make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" PFACT-1

PARAMS[PFACT]=2
make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" PFACT-2

PARAMS[NBMIN]=1
make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" NBMIN-1

PARAMS[NBMIN]=2
make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" NBMIN-2

PARAMS[NBMIN]=3
make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" NBMIN-3

PARAMS[NBMIN]=4
make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" NBMIN-4

PARAMS[NBMIN]=5
make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" NBMIN-5
```

Our test recommendations are a simple sequence. Given a set of different configurations X (compilers, libraries, etc) that you want to test:

1. Choose a set of parameters P to use in all tests. Use small values for N (about 40680 -> ~16GB) that won't affect your result and will run in a small interval.
2. Run a simple test T times for each configuration, and analyze the means/medians/what you want to use as metric.
3. Choose your best configuration C.
4. For the same configuration C and parameters P, given that you have N cores per socket, run two tests: **a)** 1 process per core, 1 thread/process, and **b)** 1 process per socket, N threads/process.

Remember to set the `OMP_PLACES` variable properly. If you're using the script, the `make_test` function uses the fourth parameter to set the variable for you.

5. Finally, tests the parameters with small sets of possible values:

The `PFACT` and `RFACT` parameters are probably architecture-dependent, as they decide the factorization algorithm for the HPL benchmark. We're still not sure if they have any influence in the other benchmarks.

The `NBMIN` and `NDIV` parameters started to show better results for values of 5 and above, but with a non-linear and not-ascending behaviour.

**NOTE THAT EACH TEST SET WILL RUN 3 TIMES BY DEFAULT, AND EACH RUN TAKES ABOUT 6-7 MINUTES ON TWO NODES FOR A N=40680 (16GB) PROBLEM SIZE!**

## ABOUT RESULTS

In general, the parameters that shows better results in HPL will have good results in most benchmarks, except for RandomAccess, which will be almost the opposite.