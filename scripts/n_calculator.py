
import sys
import math

if len(sys.argv) >= 4:
	MEM = int(sys.argv[1])
	NB = int(sys.argv[2])
	PERCENTAGE = float(sys.argv[3])
	MEM = MEM * 1024 * 1024 * 1024 # MEM in bytes
	MEM = MEM / 8 # How many double-precision fits the memory
	MEM = math.sqrt(MEM) * PERCENTAGE # Gets the N that fits the memory and gets the desired percentage
	N = MEM - (MEM % NB) # Calculates N and rounds to the nearest NB multiple
	print int(N)
else:
	print "Usage: python n_calculator.py <MEMORY_PER_NODE_IN_GBs> <DESIRED_NB> <DESIRED_MEM_PERCENTAGE_FROM_0_TO_1>"
