#!/bin/bash

###########################################################################
# WHAT THIS SCRIPT DOES?
# It plots the data of each benchmark in the .svg or .png format
# By default, it uses boxes (bars)
# 
# RUN "./plot.sh --help" if you want to configure something
# 
# Note: You may need to run the 'prepare_data.sh' script before 
# this one if you want the average results of a set of runs.
# 
# This script only works with 'env.sh' placed in the same folder!         #
#
# You may need to set the GNUPLOT_SCRIPT_DUMMY variable if 
# the dummy file is not in the same folder as this script.
###########################################################################

. env.sh

# Prefix for .svg output files
OUTPUT_PREFIX=""

# Where the images should be put
OUTPUT_FOLDER=$DATA_FOLDER/img

# Default extension for output files
OUTPUT_EXTENSION="svg"

# Type of plot to use (boxes or lines)
PLOT_TYPE='boxes'

# Self explanatory
PLOT_COLOR='#8A2BE2'

# Decide if you want to save the generated data
SAVE_DATA=0

# Name of the rendered gnuplot script (doesn't really matter)
GNUPLOT_SCRIPT=gnuplot.script

# Name of the dummy script for gnuplot
GNUPLOT_SCRIPT_DUMMY=$SCRIPT_FOLDER/dummies/gnuplot.script.dummy

# Pattern used for each set of files (you can use Regex)
DATA_PATTERNS=("^mean_impi_gcc_\d+p_\d+t" "^mean_impi_icc_\d+p_\d+t" "^mean_ompi_gcc_\d+p_\d+t" "^mean_ompi_icc_\d+p_\d+t")

while [[ $# -gt 0 ]]
do
	key="$1"
	case $key in
		-h|--help)
			echo
			echo "Plotting script v1.0.0.0"
			echo "Note: You may need to run the 'prepare_data.sh' script before this one if you want the average results of a set of runs."
			echo "Options:"
			echo "    -c|--color|--plot-color <plot_color>"
			echo "        Sets the color of the plotted data. The script is known to work for the format '#rrggbb' or any color from https://www2.uni-hamburg.de/Wiss/FB/15/Sustainability/schneider/gnuplot/colors.htm. Defaults to '$PLOT_COLOR'."
			echo "    -t|--plot-type <plot_type>"
			echo "        Sets which type of plot should be used. The script is known to work for 'boxes' and 'lines'. Defaults to '$PLOT_TYPE'."
			echo "    -p|--prefix <output_prefix>"
			echo "        Sets a prefix string to each output file. Defaults to '$OUTPUT_PREFIX'."
			echo "    --pattern <data_pattern>"
			echo "        The script will search for the pattern set by the user. The default patterns are '${DATA_PATTERNS[@]}'"
			echo "    -e|--extension <output_extension>"
			echo "        Sets the file type for the plotted data. The extensions known to work are 'png' and 'svg'. Defaults to '$OUTPUT_EXTENSION'."
			echo "    -s|--save"
			echo "        Tell the script to NOT delete the processed files for plotting. Defaults to '$SAVE_DATA'."
			echo "    -o|--output-directory <dirname>"
			echo "        Tell the script where to place the plotted data files. Defaults to '$OUTPUT_FOLDER'."
			echo "    -d|--data-directory <dirname>"
			echo "        Tell the script where the data files are. Defaults to '$DATA_FOLDER'."
			echo
			exit
			;;
		-c|--color|--plot-color)
			PLOT_COLOR="$2"
			shift
			shift
			;;
		-t|--plot-type)
			PLOT_TYPE="$2"
			shift
			shift
			;;
		-p|--prefix)
			OUTPUT_PREFIX="$2_"
			shift
			shift
			;;
		--pattern)
			DATA_PATTERNS=($2)
			shift
			shift
			;;
		-e|--extension)
			OUTPUT_EXTENSION="$2"
			shift
			shift
			;;
		-s|--save)
			SAVE_DATA=1
			shift
			;;
		-o|--output-directory)
			OUTPUT_FOLDER="$2"
			shift
			shift
			;;
		-d|--data-directory)
			DATA_FOLDER="$2"
			shift
			shift
			;;
		*)
			shift
			;;
	esac
done

if [ ! -d $OUTPUT_FOLDER ];
then
	mkdir $OUTPUT_FOLDER -p
fi

cd $DATA_FOLDER

echo "Plotting data that matches pattern(s) '${DATA_PATTERNS[@]}'..."

rm -f *.gnuplot.out

# for each benchmark
for b in "${!INFO[@]}";
do
	benchmark=$(benchmark_name "${INFO[$b]}")
	measure=$(benchmark_measure "${INFO[$b]}")
	OUTPUT=$OUTPUT_PREFIX$benchmark.gnuplot.out

	# get the output for each test set that matches the given patterns
	for pattern in "${DATA_PATTERNS[@]}";
	do
		# and put it into a single file
		for i in $(ls -1 | grep -P "$pattern");
		do
			echo "$(echo $i | sed -e 's/.txt//g' | sed -e 's/_/-/g') $(cat $i | grep -P "$b" | grep -o -P "[\d\.]+")" >> $OUTPUT
		done
	done

	cp $GNUPLOT_SCRIPT_DUMMY $GNUPLOT_SCRIPT
	sed -i -e "s/<plot_type>/$PLOT_TYPE/g" $GNUPLOT_SCRIPT	
	sed -i -e "s/<plot_color>/$PLOT_COLOR/g" $GNUPLOT_SCRIPT	
	sed -i -e "s/<output_extension>/$OUTPUT_EXTENSION/g" $GNUPLOT_SCRIPT
	sed -i -e "s/<output_filename>/$OUTPUT_PREFIX$benchmark/g" $GNUPLOT_SCRIPT
	sed -i -e "s/<output_data>/$OUTPUT/g" $GNUPLOT_SCRIPT
	sed -i -e "s/<benchmark>/$benchmark/g" $GNUPLOT_SCRIPT
	sed -i -e "s/<benchmark_measure>/$measure/g" $GNUPLOT_SCRIPT	
	gnuplot < $GNUPLOT_SCRIPT
done;

rm $GNUPLOT_SCRIPT

if [ $SAVE_DATA -eq 0 ]; then
	rm -f *.gnuplot.out
fi

mv *.$OUTPUT_EXTENSION $OUTPUT_FOLDER

echo "Plotted files created at '$OUTPUT_FOLDER'"
echo "If your graphics seems odd, rerun the script with the '-s' option and check the '.gnuplot.out' files in '$OUTPUT_FOLDER'"

cd $ORIGINAL_PATH

