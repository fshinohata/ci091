#!/bin/bash

###########################################################################
# WHAT THIS SCRIPT DOES?
# It analyzes the output data (e.g. the hpccoutf.txt files that you renamed
# and saved elsewhere) which name matches the pattern you wish (e.g. impi_gcc.*),
# and calculates the means for every set of runs (e.g. if you ran each test set 3
# times, the script will calculate the mean for those three runs)
# 
# RUN "./prepare_data.sh --help" if you want to configure something
# 
# This script only works with 'env.sh' placed in the same folder!         #
###########################################################################

. env.sh

# Where to place the output files
OUTPUT_FOLDER=$SCRIPT_FOLDER/../output

# How many times each test set ran
RUNS_PER_TEST=3

# Pattern used for each set of files (you can use Regex)
DATA_PATTERNS=("^impi_gcc_\d+p_\d+t" "^impi_icc_\d+p_\d+t" "^ompi_gcc_\d+p_\d+t" "^ompi_icc_\d+p_\d+t")

while [[ $# -gt 0 ]]
do
	key="$1"
	case $key in
		-h|--help)
			echo
			echo "Data average calculator script v1.0.0.0"
			echo "Options:"
			echo "    --pattern <data_pattern>"
			echo "        The script will search for the pattern set by the user. The default patterns are '${DATA_PATTERNS[@]}'"
			echo "    -r|--runs <number_of_runs>"
			echo "        Tell the script how many times each test was executed. Note that the order of the files must be contiguous (e.g. if you ran 3 tests, an 'ls -1' must show the 3 files sequentially). Defaults to '$RUNS_PER_TEST'."
			echo "    -o|--output-directory <dirname>"
			echo "        Tell the script where to place the processed data files. Defaults to '$OUTPUT_FOLDER'."
			echo "    -d|--data-directory <dirname>"
			echo "        Tell the script where the data files are. Defaults to '$DATA_FOLDER'."
			echo
			exit
			;;
		--pattern)
			DATA_PATTERNS=($2)
			shift
			shift
			;;
		-r|--runs)
			RUNS_PER_TEST=$2
			shift
			shift
			;;
		-o|--output-directory)
			OUTPUT_FOLDER="$2"
			shift
			shift
			;;
		-d|--data-directory)
			DATA_FOLDER="$2"
			shift
			shift
			;;
		*)
			shift
			;;
	esac
done


declare -A MEAN

function reset_means () {
	for b in "${!INFO[@]}";
	do
		let MEAN["$b"]=0
	done
}

reset_means

for pattern in "${DATA_PATTERNS[@]}";
do
	let k=1
	echo "Searching for pattern '$pattern'"
	for i in $(ls -1 $DATA_FOLDER | grep -P "$pattern");
	do
		echo "analyzing $i..."
		for b in "${!INFO[@]}";
		do
			MEAN["$b"]="$( echo "scale=4 ; $(perl $HPCC_COMPILER -w -f $DATA_FOLDER/$i | grep -P "$b" | grep -o -P "\d+\.\d+") + ${MEAN["$b"]}" | bc )"
		done

		let k=k+1
		if [ $k -gt $RUNS_PER_TEST ];
		then
			echo "Saving means for $( echo $i | sed -re "s/_run-[0-9]+//g" )..."
			mean_file=$OUTPUT_FOLDER/mean_$( echo $i | sed -re "s/_run-[0-9]+//g" ).txt

			rm -f $mean_file
			
			for b in "${!INFO[@]}";
			do
				MEAN["$b"]="$( echo "scale=4 ; ${MEAN[$b]} / $RUNS_PER_TEST" | bc )"
				echo "MEAN[\"$b\"] = ${MEAN["$b"]} $(benchmark_measure "${INFO["$b"]}" | sed -e 's/\\//g')"
				echo "$b ${MEAN["$b"]}" >> $mean_file	
			done

			echo
			echo

			let k=1
			reset_means 
		fi
	done
done
