#!/bin/bash

# From where the script was ran
ORIGINAL_PATH=$(pwd)

# Absolute path of the running script
SCRIPT_FOLDER="$(cd $(dirname "$0") ; pwd -P)"

# Where the HPCC main folder is
HPCC_FOLDER=$SCRIPT_FOLDER/../hpcc

# Where the data is (by "data" I mean "the hpccoutf.txt which you renamed and saved elsewhere")
# The 'hpcc.sh' script is configured to send the output files to this folder by default
DATA_FOLDER=$SCRIPT_FOLDER/../output

# Where the HPCC compiler.pl file is
HPCC_COMPILER=$SCRIPT_FOLDER/compiler.pl

declare -A INFO
INFO[G-HPL]="HPL TFlops\/s"
INFO[G-PTRANS]="PTRANS TBytes\/s"
INFO[G-RandomAccess]="RandomAccess GUPS"
INFO[G-FFT]="FFT TFlops\/s"
INFO["EP-STREAM Sys"]="STREAM TBytes\/s"
INFO[EP-DGEMM]="DGEMM GFlops\/s"
INFO["RandomRing Bandwidth"]="CommunicationBandwidth GBytes\/s"
INFO["RandomRing Latency"]="CommunicationLatency usec"

function benchmark_name () {
	echo "$(echo "$1" | sed -e "s/ .*//g")"
}

function benchmark_measure () {
	echo "$(echo "$1" | sed -e "s/.* //g")"
}

