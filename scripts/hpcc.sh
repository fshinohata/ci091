#!/bin/bash

# Instructions for SBATCH (describe your cluster here)
#SBATCH --job-name=hpcc
#SBATCH --chdir=~/slurm-jobs/
#SBATCH --nodelist=hpc1-ib,hpc2-ib
#SBATCH --wait-all-nodes=1
#SBATCH --cpu-freq=Performance

# Check the SBATCH info above and set them as needed


####################################################
#
# If you want everything to work nicely, you need
# to respect the following folder-tree structure:
# 
# PARENT_FOLDER/
#   |
#   |-- hpcc/       HPCC Benchmark main folder
#   |
#   |-- output/     Empty folder where the test
#   |               results will be placed
#   |-- scripts/
#         |
#         |-- hpcc.sh           This script
#         |-- env.sh
#         |-- plot.sh
#         |-- prepare_data.sh
#         |-- n_calculator.py
#         |-- compiler.pl
#         
#   I'll call this folder tree "The Tree"
#   so you don't get lost reading the rest
#   
#   Every variable that may need a custom
#   configuration will be inside a 'comment block'
#
####################################################

####################################################
# BEGIN BLOCK (WHAT YOU NEED TO CONFIGURE)
####################################################

module load gcc/7.3.0
module load mkl/2018.1

# General info
HOME="$HOME/HPCC"             # The PARENT_FOLDER of The Tree
ORIGINAL_PATH=$(pwd)          # Doesn't matter
OUTPUT_FOLDER="$HOME/output"  # The output/ folder of The Tree

# Info about nodes
NUM_NODES=2                   # Change this inside the function test_battery() !
NODELIST="hpc1-ib,hpc2-ib"    # Comma-separated list of nodes
                              # The first one is considered to be the master node!
                              # Also, the master node is the one who should run this script.

# Extra scripts
SCRIPT_FOLDER="$HOME/scripts" # The scripts/ folder of The Tree

# Test info
RUNS_PER_TEST=3               # How many times each test set should be executed
                              # Remember to copy whatever you set here to the 'prepare_data.sh'
                              # script (it has the very same variable) or use the -r option
                              # when running the script

# Benchmark info
HPCC_FOLDER="$HOME/hpcc"      # The hpcc/ folder of The Tree
HPCC="$HPCC_FOLDER/hpcc"
HPCCINF="$HPCC_FOLDER/hpccinf.txt"
HPCCOUTF="$HPCC_FOLDER/hpccoutf.txt"

####################################################
# END BLOCK (WHAT YOU NEED TO CONFIGURE)
####################################################




# Variables set by load_env()
ENVIRONMENT=""
OUTPUT_PREFIX=""
MAKEFILE_ARCH=""
SCHEDULER=""
OUTPUT=""
MPIRUN=""
SRUN=""

# Permanent environment variables
export OMP_DISPLAY_ENV=true

# Select which 'environment' to use
# Options: 
# IMPI_GCC_MKL
# IMPI_ICC_MKL
# OMPI_GCC_MKL
# OMPI_ICC_MKL
function load_env () {

    ENVIRONMENT="$1"

    ####################################################
    # BEGIN BLOCK (WHAT YOU NEED TO CONFIGURE)
    ####################################################
    # Rewrite the IFs with whatever configuration you need
    # 
    # The argument $1 will be given by you in the end of this script,
    # so make whatever comparisons you want
    # 
    # The variables you need to set are:
    # OUTPUT_PREFIX => The prefix for each output file of that environment
    # MAKEFILE_ARCH => The name of the HPL makefile that has the configuration
    #   of the environment
    # SCHEDULER        => The job scheduler to use (srun, mpirun, mpiexec.hydra, etc)
    # 
    # Any other variable is either useless or USELESS for the rest of the script
    
    if [ "$1" == "IMPI_GCC_MKL" ]; then

        module unload ompi/3.0.0
        module load impi/2018.1
        OUTPUT_PREFIX="impi_gcc"
        MAKEFILE_ARCH="INTELMPI_GCC_MKL" 
        
        echo "[LOADED INTELMPI + GCC + MKL]"
        MPIRUN="mpiexec.hydra -genv I_MPI_DEBUG 5"
        SRUN=$MPIRUN # Slurm can't do the job!
        SCHEDULER=$MPIRUN

    elif [ "$1" == "IMPI_ICC_MKL" ]; then

        module unload ompi/3.0.0
        module load impi/2018.1
        OUTPUT_PREFIX="impi_icc"
        MAKEFILE_ARCH="INTELMPI_ICC_MKL" 

        echo "[LOADED INTELMPI + ICC + MKL]"
        MPIRUN="mpiexec.hydra -genv I_MPI_DEBUG 5"
        SRUN=$MPIRUN # Slurm can't do the job!
        SCHEDULER=$MPIRUN

    elif [ "$1" == "OMPI_GCC_MKL" ]; then

        module unload impi/2018.1
        module load ompi/3.0.0
        OUTPUT_PREFIX="ompi_gcc"
        MAKEFILE_ARCH="OPENMPI_GCC_MKL"

        echo "[LOADED OPENMPI + GCC + MKL]"
        SRUN="srun --mpi=pmi2" # Doesn't allow precise mapping
        MPIRUN="mpirun --report-bindings -x OMP_PLACES -x OMP_PROC_BIND -x MKL_NUM_THREADS -x OMP_NUM_THREADS -x OMP_DISPLAY_ENV -x LD_LIBRARY_PATH"
        SCHEDULER=$MPIRUN

    elif [ "$1" == "OMPI_ICC_MKL" ]; then

        module unload impi/2018.1
        module load ompi/3.0.0
        OUTPUT_PREFIX="ompi_icc"
        MAKEFILE_ARCH="OPENMPI_ICC_MKL"

        echo "[LOADED OPENMPI + ICC + MKL]"
        SRUN="srun --mpi=pmi2" # Doesn't allow precise mapping
        MPIRUN="mpirun --report-bindings -x OMP_PLACES -x OMP_PROC_BIND -x MKL_NUM_THREADS -x OMP_NUM_THREADS -x OMP_DISPLAY_ENV -x LD_LIBRARY_PATH"
        SCHEDULER=$MPIRUN

    fi

    # Make whatever export you need here, if something goes wrong
    # Erase'em all if you're certain that everything is ok
    
    # These Libraries exports need to be reloaded every time a module is unloaded/loaded,
    # as 'modules' rewrites the LD_LIBRARY_PATH variable
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cluster/parallel_studio/2018.1/compilers_and_libraries_2018.1.163/linux/compiler/lib/intel64/
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cluster/parallel_studio/2018.1/compilers_and_libraries_2018.1.163/linux/mkl/lib/intel64/

    # MPIRUN thread/process pinning policy
    export OMP_PROC_BIND=close                   # Pin related processes/threads as close as possible
    
    ####################################################
    # END BLOCK (WHAT YOU NEED TO CONFIGURE)
    ####################################################

    OUTPUT="$OUTPUT_FOLDER/${OUTPUT_PREFIX}_output.txt"
}




####################################################
# BEGIN BLOCK (WHAT YOU NEED TO CONFIGURE)
####################################################
# These are the default parameters to use in the tests
# You can change some of them inside the function test_battery()
# to do a parameter testing

# Benchmark Parameters (Change them every time you need)
declare -A PARAMS
PARAMS[N]=46080               # Problem size (32768 == 8GB, 46080 == 16GB) USE the n_calculator.py to calculate this parameter if you want to use a non-2-power NB
PARAMS[NB]=512                # Block size (affects which N can be used, use the n_calculator.py !)
PARAMS[PMAP]=0                # Process Mapping (0 = Row-major, 1 = Column Major)
PARAMS[P]=1                   # Process-grid P (P must be slightly smaller than Q)
PARAMS[Q]=1                   # Process-grid Q (PxQ == number of processes to run)
PARAMS[PFACT]=1               # Panel Facts
PARAMS[NBMIN]=8        
PARAMS[NDIV]=6
PARAMS[RFACT]=2               # Recursive Panel Facts
PARAMS[BCAST]=1               # Broadcast type (1 = Increasing-ring modified, said to be the best in most cases)
PARAMS[DEPTH]=1               # Look-ahead depth (1 is said to be the best in most cases)
PARAMS[SWAP]=2                # Swapping type
PARAMS[SWAP_THRESHOLD]=16
PARAMS[L1]=1                  # L1 Matrix state (0 = Transposed, 1 = Not Transposed)
PARAMS[U]=0                   # U  Matrix state (0 = Transposed, 1 = Not Transposed)
PARAMS[EQUILIBRATION]=1       # Problem distribution equilibration (1 = yes, 0 = no)
PARAMS[MEM_ALIGN]=8           # Memory Alignment (in Bytes)

# Dummy hpccinf.txt, with pseudo-values for replacing with 'sed'
# If you respected The Tree, these guys should be good to go.
DUMMYINF_FOLDER="$SCRIPT_FOLDER/dummies"
DUMMYINF="$DUMMYINF_FOLDER/dummyinf.txt"

####################################################
# END BLOCK (WHAT YOU NEED TO CONFIGURE)
####################################################

# Global variable for number of processes
NUM_PROCESSES=""


function to_output () {
    echo -e "$1" >> $OUTPUT
}

function compile () {
    cd $HPCC_FOLDER
    make arch=$MAKEFILE_ARCH clean all

    #########################################
    scp -r $HPCC_FOLDER hpc2:$HPCC_FOLDER   # ERASE THIS IF YOU'RE USING SHARED FOLDERS (IF NOT, ADAPT IT)
    #########################################
    
    cd $ORIGINAL_PATH
}

# run(string output_filename)
function run () {
    cd $HPCC_FOLDER

    rm -f $HPCCOUTF
    cp $DUMMYINF $HPCCINF
    
    NODES_TO_USE="1"

    for((i = 1; i < $NUM_NODES; i++)); do
        NODES_TO_USE+=",$(($i+1))"
    done

    NODES_TO_USE=$(echo $NODELIST | cut -f$NODES_TO_USE -d',')

    to_output "------------------------------------------------------"
    to_output "TEST RUN:"
    to_output "[Date: $(date)]"
    to_output "[$NUM_PROCESSES PROCESSES, $MKL_NUM_THREADS THREADS]"

    to_output "NODES=$NODES_TO_USE"
    to_output "NUM_THREADS=$MKL_NUM_THREADS"
    to_output "NUM_PROCESSES=$NUM_PROCESSES"
    to_output "OMP_PROC_BIND=$OMP_PROC_BIND"
    to_output "OMP_PLACES=$OMP_PLACES"
    to_output "\nPARAMETERS:"

    for I in "${!PARAMS[@]}"; do
        sed -i -e "s/<$I>/${PARAMS[$I]}/g" $HPCCINF
        to_output "$I\t\t\t= ${PARAMS[$I]}"
    done;

    ################################
    scp $HPCCINF hpc2:$HPCCINF     # ERASE THIS IF YOU'RE USING SHARED FOLDERS (IF NOT, ADAPT IT)
    ################################



    ####################################################
    # BEGIN BLOCK (WHAT YOU NEED TO CONFIGURE)
    ####################################################
    # Each 'if' should contain the proper execution line for
    # each scheduler used. In the original script,
    # we have two INTEL MPI environments, in which
    # 
    #   SCHEDULER = 'mpiexec.hydra <default_options>'
    # 
    # and two OPEN MPI environments, in which
    # 
    #   SCHEDULER = 'mpirun <default_options>'
    # 
    # So we use the 'expr' program to check if the IMPI or OMPI prefix
    # is present to know which options to use to select the nodes
    # and the amount of processes to launch.
    
    # This one for mpiexec.hydra (intel mpi)
    if [ $(expr "$ENVIRONMENT" : '^IMPI') -gt 0 ]; then

        ( time $SCHEDULER -hosts $NODES_TO_USE -perhost $(($NUM_PROCESSES / $NUM_NODES)) $HPCC ) 2>> $OUTPUT >> $OUTPUT
    
    # This one for mpirun (open mpi)
    elif [ $(expr "$ENVIRONMENT" : '^OMPI') -gt 0 ]; then
    
        ( time $SCHEDULER --host $NODES_TO_USE -N $(($NUM_PROCESSES / $NUM_NODES)) $HPCC ) 2>> $OUTPUT >> $OUTPUT
    
    fi
    
    ####################################################
    # END BLOCK (WHAT YOU NEED TO CONFIGURE)
    ####################################################



    to_output "\n\n$(perl compiler.pl -w -f $HPCCOUTF)"

    if [ "$1" != "" ]; then
        cp $HPCCOUTF $OUTPUT_FOLDER/$1
    fi;

    cd $ORIGINAL_PATH
}

# make_test(int P, int Q, int NTHREADS, string OMP_PLACES, string output_suffix)
function make_test () {
    PARAMS[P]=$1
    PARAMS[Q]=$2
    NUM_PROCESSES=$((PARAMS[P] * PARAMS[Q]))
    export MKL_NUM_THREADS=$3
    export OMP_NUM_THREADS=$3
    export OMP_PLACES=$4

    # Default mapping
    if [ "$4" == "" ]; then
        export OMP_PLACES=cores
    fi

    OUTPUT_FILENAME=${OUTPUT_PREFIX}_${NUM_PROCESSES}p_${3}t

    if [ "$5" != "" ]; then
        OUTPUT_FILENAME+="_$5"
    fi

    for (( i = 1; i <= $RUNS_PER_TEST; i++ )); do
        # Name of each output file (change it if you want to)
        # If you change the _run-$i suffix, be sure to change it
        # in the script 'prepare_data.sh' too! 
        # (lines 98 and 99, in 'sed')
        run ${OUTPUT_FILENAME}_run-$i.txt
    done
}

# test_battery()
function test_battery () {

    ####################################################
    # BEGIN BLOCK (WHAT YOU NEED TO CONFIGURE)
    ####################################################
    # This function has the test_battery to run for the
    # selected environment.
    # 
    # In short, you set the number of nodes to use in the
    # subsequential tests (NUM_NODES=<n>) and run them.
    # 
    # You can change the PARAMS[] array here. Each parameter
    # changed will affect the subsequential tests.
    # 
    # To run a test, use the make_test() function, which
    # signature is:
    # 
    #   make_test(int P, int Q, int NTHREADS, string OMP_PLACES, string scheduler_extra_options)
    # 
    # Where:
    #   P             => PARAMS[P] (P of the PxQ grid)
    #   Q             => PARAMS[Q] (Q of the PxQ grid)
    #                    The number of processes will be P * Q
    #                              
    #   NTHREADS      => Number of threads per process
    #   
    #   OMP_PLACES    => The OMP_PLACES variable
    #                    if not given, defaults to 'core'
    #                              
    #   output_suffix => An unique suffix for your output file
    #                    By default, the output file name will
    #                    be something like 
    #                    (ENVIRONMENT_PREFIX)_(NPROC)p_(NTHREAD)t_run-(NRUN).txt
    #                    impi_icc_8p_8t_run-1.txt
    #                    With the suffix, the filename will be
    #                    (ENVIRONMENT_PREFIX)_(NPROC)p_(NTHREAD)t_(YOUR_SUFFIX)_run-(NRUN).txt
    #                    impi_icc_8p_8t_nb224_run-1.txt
    
    NUM_NODES=1

    ############################################
    # TRIVIAL TEST

    # (1 process, 1 thread/p) DONE
    # make_test 1 1 1



    ############################################
    # TESTS WITH N THREADS PER PROCESS

    # (4 processes, 8 threads/p)
    # make_test 1 4 8 "{0:8},{8:8},{16:8},{24:8}"

    # (8 processes, 4 threads/p)
    # make_test 2 4 4 "{0:4},{8:4},{16:4},{24:4},{4:4},{12:4},{20:4},{28:4}"

    # (16 processes, 2 threads/p)
    # make_test 4 4 2 "{0:2},{8:2},{16:2},{24:2},{2:2},{10:2},{18:2},{26:2},{4:2},{12:2},{20:2},{28:2},{6:2},{14:2},{22:2},{30:2}"

    # (32 processes, 1 thread/p) DONE?
    # make_test 4 8 1 cores

    ############################################
    # TESTS WITH 2 NODES

    NUM_NODES=2
    
    # (8 processes, 8 threads/p)
    PARAMS[PFACT]=0
    make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" PFACT-0

    PARAMS[PFACT]=1
    make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" PFACT-1

    PARAMS[PFACT]=2
    make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" PFACT-2
    
    # (8 processes, 8 threads/p)
    # make_test 2 4 8 "{0:8},{8:8},{16:8},{24:8}" 2-4
    
    # (8 processes, 8 threads/p)
    # make_test 1 8 8 "{0:8},{8:8},{16:8},{24:8}" 1-8

    # (64 processes, 1 thread/p)
    # make_test 8 8 1 cores 8-8
    
    # (64 processes, 1 thread/p)
    # make_test 4 16 1 cores 4-16

    ####################################################
    # END BLOCK (WHAT YOU NEED TO CONFIGURE)
    ####################################################

}

# run_test_battery(string environment)
run_test_battery () {
    load_env "$1"
    compile
    test_battery
}



####################################################
# BEGIN BLOCK (WHAT YOU NEED TO CONFIGURE)
####################################################
# After configuring the test battery, select which
# environments will be used for them.
# 
# The environments are the ones you set in the load_env()
# function

# IMPI + ICC + MKL
run_test_battery IMPI_ICC_MKL

# IMPI + GCC + MKL
run_test_battery IMPI_GCC_MKL

# OMPI + GCC + MKL
# run_test_battery OMPI_GCC_MKL

# OMPI + ICC + MKL
# run_test_battery OMPI_ICC_MKL

####################################################
# END BLOCK (WHAT YOU NEED TO CONFIGURE)
####################################################
